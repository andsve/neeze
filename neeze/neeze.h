#ifndef NEEZE_H
#define NEEZE_H
#include <stdint.h>

namespace neeze
{

    enum LOG_LEVEL
    {
        LOG_LEVEL_INFO,
        LOG_LEVEL_DEBUG,
        LOG_LEVEL_WARNING,
        LOG_LEVEL_ERROR,
    };

    enum NES_OP_ADDR_MODES {
        Const                = 0,  // _const  imm = #$00
        Absolute             = 1,  // _abs    abs = $0000
        Absolute_ZP          = 2,  // _zp      zp = $00
        Accumulator          = 3,  // _a
        Index_X              = 4,  // _abs_x  abx = $0000,X
        Index_Y              = 5,  // _abs_y  aby = $0000,Y
        Index_ZP_X           = 6,  // _zp_x   zpx = $00,X
        Index_ZP_Y           = 7,  // _zp_y   zpy = $00,Y
        Indirect             = 8,  // _ind    ind = ($0000)
        PreIndex_Indirect_X  = 9,  // _ind_x  izx = ($00,X)
        PostIndex_Indirect_Y = 10, // _ind_y  izy = ($00),Y
        Relative             = 11, //         rel = $0000
        No_Address           = 12, //
        Unused               = 13  //
    };

    struct cpu_t;

    // Callback typedefs
    typedef void (*cb_log_t)(LOG_LEVEL level, const char* log);
    typedef void (*cb_debug_fetch_instr_t)(cpu_t& cpu, uint8_t instruction, uint32_t ticks);
    typedef void (*cb_debug_addr_mode_t)(cpu_t& cpu, NES_OP_ADDR_MODES mode);
    typedef void (*cb_debug_mem_read_t)(cpu_t& cpu, uint16_t addr1, uint16_t addr2, uint16_t data);
    typedef void (*cb_debug_mem_write_t)(cpu_t& cpu, uint16_t addr, uint8_t prev_data, uint8_t new_data);
    typedef void (*cb_debug_reg_write_t)(cpu_t& cpu, char reg);
    typedef void (*cb_debug_pre_exec_t)(cpu_t& cpu);
    typedef void (*cb_debug_post_exec_t)(cpu_t& cpu);
    typedef void (*cb_debug_instr_done_t)(cpu_t& cpu);

    enum RESULT
    {
        RESULT_STEP_OK,
        RESULT_STEP_ERROR,
        RESULT_ROM_LOAD_OK,
        RESULT_ROM_LOAD_ERROR,
        RESULT_UNKNOWN_ERROR
    };

    struct cpu_t
    {
        // Registers
        uint8_t reg_P;
        uint8_t reg_A, reg_X, reg_Y;
        uint16_t reg_PC;
        uint8_t reg_S;

        // RAM and Stack
        char ram[0x700];
        char stack[0x100];

        // Currently mapped prg rom banks
        char* prgrom_lower;
        char* prgrom_upper;

        char _4015;
    };

    struct apu_t
    {
        //
    };

    struct ppu_t
    {
        uint8_t ctrl;   // PPUCTRL $2000   VPHB SINN   NMI enable (V), PPU master/slave (P), sprite height (H), background tile select (B), sprite tile select (S), increment mode (I), nametable select (NN)
        uint8_t mask;   // PPUMASK $2001   BGRs bMmG   color emphasis (BGR), sprite enable (s), background enable (b), sprite left column enable (M), background left column enable (m), greyscale (G)
        uint8_t status; // PPUSTATUS   $2002   VSO- ----   vblank (V), sprite 0 hit (S), sprite overflow (O), read resets write pair for $2005/2006
        // OAMADDR $2003   aaaa aaaa   OAM read/write address
        // OAMDATA $2004   dddd dddd   OAM data read/write
        // PPUSCROLL   $2005   xxxx xxxx   fine scroll position (two writes: X, Y)
        // PPUADDR $2006   aaaa aaaa   PPU read/write address (two writes: MSB, LSB)
        // PPUDATA $2007   dddd dddd   PPU data read/write
        // OAMDMA  $4014   aaaa aaaa   OAM DMA high address

        // rendering pointers
        uint16_t x, y;

        // framebuffer
        uint8_t fb[256*240*3];
    };

    struct ines_rom_t
    {
        uint8_t prg_page_count;
        uint8_t chr_page_count;
        uint8_t ram_page_count;

        char** prg_pages;
        char** chr_pages;

        // |   6    |  1   | ROM Control Byte #1                      |
        // |        |      |   %####vTsM                              |
        // |        |      |    |  ||||+- 0=Horizontal Mirroring      |
        // |        |      |    |  ||||   1=Vertical Mirroring        |
        // |        |      |    |  |||+-- 1=SRAM enabled              |
        // |        |      |    |  ||+--- 1=512-byte trainer present  |
        // |        |      |    |  |+---- 1=Four-screen VRAM layout   |
        // |        |      |    |  |                                  |
        // |        |      |    +--+----- Mapper # (lower 4-bits)     |
        // FLAGS
        bool mirroring_vertical;
        bool battery_ram;
        bool trainer;
        bool mirroring_fourscreen;
        uint8_t mapper_id;

        ines_rom_t() {
            prg_pages = 0x0;
        }
    };

    struct sys_t
    {
        // callbacks
        cb_log_t               cb_log;
        cb_debug_fetch_instr_t cb_debug_fetch_instr;
        cb_debug_addr_mode_t   cb_debug_addr_mode;
        cb_debug_reg_write_t   cb_debug_reg_write;
        cb_debug_mem_write_t   cb_debug_mem_write;
        cb_debug_mem_read_t    cb_debug_mem_read;
        cb_debug_pre_exec_t    cb_debug_pre_exec;
        cb_debug_post_exec_t   cb_debug_post_exec;
        cb_debug_instr_done_t  cb_debug_instr_done;

        cpu_t cpu;
        ppu_t ppu;

        sys_t() {
            cb_log = 0x0;
            cb_debug_fetch_instr = 0x0;
            cb_debug_addr_mode = 0x0;
            cb_debug_reg_write = 0x0;
            cb_debug_mem_write = 0x0;
            cb_debug_mem_read = 0x0;
            cb_debug_pre_exec = 0x0;
            cb_debug_instr_done = 0x0;
            cb_debug_post_exec = 0x0;
        }
    };


    RESULT load_rom_file(sys_t& sys, const char *filepath, ines_rom_t& out_rom);
    RESULT load_rom_mem(sys_t& sys, const char* data, long int size, ines_rom_t& out_rom);
    void init(sys_t& sys);
    RESULT step(sys_t& sys, uint32_t cycles);

    // sys_t create_system();
}

#endif
