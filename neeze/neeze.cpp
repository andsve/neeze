#include <cstdio>
#include <cstring>
#include <cerrno>
#include <stdarg.h>
#include <stdlib.h>

#include "neeze.h"
#include "neeze_private.h"
#include "neeze_mem.h"

using namespace neeze;

const char _ines_magic[4] = {'N', 'E', 'S', 0x1A}; // "NES" followed by MS-DOS end-of-file

RESULT neeze::load_rom_file(sys_t& sys, const char *filepath, ines_rom_t& out_rom)
{
    LOG_D("Loading iNES ROM '%s'", filepath);

    FILE* fp = fopen(filepath, "rb");
    long int file_size;

    if (fp == 0x0) {
        LOG_E("Could not open iNES ROM file '%s', reason: %s", filepath, strerror(errno));
        return RESULT_ROM_LOAD_ERROR;
    }

    fseek(fp, 0, SEEK_END);
    file_size = ftell(fp);
    rewind(fp);
    LOG_D("iNES ROM size: %ld bytes.", file_size);

    char* data = (char*)malloc(file_size * sizeof(char));
    fread(data, sizeof(char), file_size, fp);
    fclose(fp);

    RESULT res = load_rom_mem(sys, data, file_size, out_rom);
    (void)res;

    free(data);

    return RESULT_ROM_LOAD_OK;
}

void neeze::print_bits(uint8_t x)
{
    int i;
    for(i=8*sizeof(x)-1; i>=0; i--) {
        (x & (1 << i)) ? putchar('1') : putchar('0');
    }
    printf("\n");
}

// iNES format information:
// https://wiki.nesdev.com/w/index.php/INES
// http://fms.komkon.org/EMUL8/NES.html#LABM
// http://nesdev.com/iNES.txt
// http://nesdev.com/neshdr20.txt
RESULT neeze::load_rom_mem(sys_t& sys, const char* data, long int size, ines_rom_t& out_rom)
{
    // Verify iNES header
    if (strncmp(data, _ines_magic, 4) == 0)
    {
        LOG_D("Found correct iNES header magic.");

        // Continue parsing rest of iNES header
        out_rom.prg_page_count = data[4];
        out_rom.chr_page_count = data[5];

        uint8_t ines_flags = data[6];
        // First flag byte
        out_rom.mirroring_vertical   = ines_flags & (0x1 << 7); LOG_D("iNES Vertical Mirroring: %s", out_rom.mirroring_vertical ? "true" : "false");
        out_rom.battery_ram          = ines_flags & (0x1 << 6); LOG_D("iNES Battery RAM: %s", out_rom.battery_ram ? "true" : "false");
        out_rom.trainer              = ines_flags & (0x1 << 5); LOG_D("iNES Trainer: %s", out_rom.trainer ? "true" : "false");
        out_rom.mirroring_fourscreen = ines_flags & (0x1 << 4); LOG_D("iNES Four-screen Mirroring: %s", out_rom.mirroring_fourscreen ? "true" : "false");
        out_rom.mapper_id            = ines_flags & 0x0F;

        if (out_rom.trainer) {
            LOG_E("Trainers not supported!");
            return RESULT_ROM_LOAD_ERROR;
        }

        // Second flag byte
        ines_flags = data[7];
        out_rom.mapper_id            = (ines_flags & (0x0F << 4)) | out_rom.mapper_id; LOG_D("iNES Mapper: %d", out_rom.mapper_id);

        // Get size of RAM bank
        out_rom.ram_page_count = data[8];
        LOG_D("iNES PRG-ROM Banks: %d", out_rom.prg_page_count);
        LOG_D("iNES CHR-ROM Banks: %d", out_rom.chr_page_count);
        LOG_D("iNES RAM Banks: %d", out_rom.ram_page_count);

        // PRG data
        const char* chr_start = &data[16];
        out_rom.prg_pages = (char**)malloc(out_rom.prg_page_count*sizeof(char*));
        for (int i = 0; i < out_rom.prg_page_count; ++i)
        {
            out_rom.prg_pages[i] = new char[16 * 1024];
            memcpy(out_rom.prg_pages[i], chr_start, 16 * 1024 * sizeof(char));
            chr_start += 16 * 1024;
        }

        // CHR data
        out_rom.chr_pages = (char**)malloc(out_rom.chr_page_count*sizeof(char*));
        for (int i = 0; i < out_rom.chr_page_count; ++i)
        {
            out_rom.chr_pages[i] = new char[8 * 1024];
            memcpy(out_rom.chr_pages[i], chr_start, 8 * 1024 * sizeof(char));
            chr_start += 8 * 1024;
        }

        if (out_rom.prg_page_count == 1)
        {
            sys.cpu.prgrom_lower = out_rom.prg_pages[0];
            sys.cpu.prgrom_upper = out_rom.prg_pages[0];
        } else {
            LOG_E("TODO Solve mapping for more than one PRG ROM bank.\n");
            return RESULT_ROM_LOAD_ERROR;
        }

    } else {
        LOG_E("Invalid iNES file header, magic does not match.");
    }

    return RESULT_ROM_LOAD_OK;
}


void neeze::init_cpu(neeze::sys_t &sys)
{
    cpu_t& cpu = sys.cpu;

    // reset cpu
    cpu.reg_PC = 0xC000;
    cpu.reg_S  = 0xFD;

    /*
    cpu.reg_P.S = 0; // Sign
    cpu.reg_P.V = 0; // Overflow
    cpu.reg_P._ = 1; // unused
    cpu.reg_P.B = 0; // Soft BRK
    cpu.reg_P.D = 0; // Decimal
    cpu.reg_P.I = 0; // Disable interrupt
    cpu.reg_P.Z = 0; // Zero
    cpu.reg_P.C = 0; // Carry
    */
    cpu.reg_P = 0x24;


    /*test_mem[0x004C] = 0x00;
    test_mem[0x004D] = 0x21;
    test_mem[0x2105] = 0x6D;*/

    cpu.reg_A = 0x0;
    cpu.reg_X = 0x0;
    cpu.reg_Y = 0x0;

    // reset RAM
    memset(cpu.ram, 0, 0x700);

}

void neeze::init(sys_t& sys)
{
    init_cpu(sys);
    init_ppu(sys);
}

RESULT neeze::step(sys_t& sys, uint32_t cycles)
{
    // On NTSC systems, the PPU divides the master clock by 4 while the CPU uses
    // the master clock divided by 12. Since both clocks are fed off the same master
    // clock, this means that there are exactly three PPU ticks per CPU cycle, with
    // no drifting over time (though the clock alignment might vary depending on
    // when you press the Reset button).
    step_cpu(sys, cycles);
    step_ppu(sys, cycles*3);
    return RESULT_STEP_OK;
}

// neeze::sys_t neeze::CreateSystem()
// {
//     return neeze::sys_t();
// }
