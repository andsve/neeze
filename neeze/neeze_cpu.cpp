#include "neeze.h"
#include "neeze_mem.h"
#include "neeze_private.h"

using namespace neeze;

uint32_t neeze::step_cpu(sys_t& sys, uint32_t max_ticks)
{
    static unsigned char  instr;
    static unsigned short data;
    static unsigned short temp;
    static unsigned short addr;
    static unsigned int   ticks = 0x0;

    unsigned int delta_ticks = 0x0;
    unsigned int start_ticks = ticks;

    cpu_t& cpu = sys.cpu;

    while (max_ticks > delta_ticks)
    {


        // fetch instructions
        instr = memory_read(cpu, cpu.reg_PC);
        if (sys.cb_debug_fetch_instr) {
            sys.cb_debug_fetch_instr(cpu, instr, ticks);
        }
        cpu.reg_PC += 0x1;

        ////////////////////////////////////////
        // address and data resolve
        //
        NES_OP_ADDR_MODES addr_mode = NES_OP_ADDR_MODE_LUT[instr];
        if (sys.cb_debug_addr_mode) {
            sys.cb_debug_addr_mode(cpu, addr_mode);
        }

        switch (addr_mode)
        {
            case Const:
                data = memory_read(cpu, cpu.reg_PC);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, 0x0, 0x0, data);
                cpu.reg_PC += 0x1;
            break;

            case Absolute:
                addr = ((unsigned short)memory_read(cpu, cpu.reg_PC+1) << 8) | memory_read(cpu, cpu.reg_PC);
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, addr, 0x0, data);
                cpu.reg_PC += 0x2;
            break;

            case Absolute_ZP:
                addr = (unsigned short)memory_read(cpu, cpu.reg_PC);
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, addr, 0x0, data);
                cpu.reg_PC += 0x1;
            break;

            case Index_X:
                addr = ((unsigned short)memory_read(cpu, cpu.reg_PC+1) << 8) | memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr += cpu.reg_X;
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x2;
            break;

            case Index_Y:
                addr = ((unsigned short)memory_read(cpu, cpu.reg_PC+1) << 8) | memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr += cpu.reg_Y;
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x2;

            break;

            case Index_ZP_X:
                addr = (unsigned short)memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr = (unsigned char)(addr + cpu.reg_X);
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x1;
            break;

            case Index_ZP_Y:
                addr = (unsigned short)memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr = (unsigned char)(addr + cpu.reg_Y);
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x1;
            break;

            case Indirect:
                addr = ((unsigned short)memory_read(cpu, cpu.reg_PC+1) << 8) | memory_read(cpu, cpu.reg_PC);
                temp = addr;
                data = addr;
                addr = addr_indirect(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x2;

                /*
                Take care of the JMP-bug on the NES cpu:
                An original 6502 has does not correctly fetch the target address if
                the indirect vector falls on a page boundary (e.g. $xxFF where xx is
                and value from $00 to $FF). In this case fetches the LSB from $xxFF
                as expected but takes the MSB from $xx00.
                */
                if ((data & 0x00FF) == 0x00FF)
                {
                    addr = ((unsigned short)memory_read(cpu, data & 0xFF00) << 8) | memory_read(cpu, data);
                }

                data = addr;
            break;

            case PreIndex_Indirect_X:
                addr = (unsigned short)memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr = addr_preindexed_indirect_zeropage(cpu, addr, cpu.reg_X);
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x1;
            break;

            case PostIndex_Indirect_Y:
                addr = (unsigned short)memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr = addr_postindexed_indirect(cpu, addr, cpu.reg_Y);
                data = memory_read(cpu, addr);
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, memory_read_short_zp_wrap(cpu, temp), addr);
                cpu.reg_PC += 0x1;
            break;

            case Relative:
                addr = memory_read(cpu, cpu.reg_PC);
                temp = addr;
                addr = cpu.reg_PC + 0x1 + (signed char)addr;
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, temp, addr, data);
                cpu.reg_PC += 0x1;
            break;

            case Accumulator:
                data = cpu.reg_A;
            case No_Address:
            case Unused:
                if (sys.cb_debug_mem_read) sys.cb_debug_mem_read(cpu, 0x0, 0x0, data);
                break;

            default:
                LOG_E("Unknown addressing mode.");
                break;
        };


        /////////////////////////////////////////////////////////
        // interpret and execute instruction
        //
        if (sys.cb_debug_pre_exec) sys.cb_debug_pre_exec(cpu);
        ticks += NES_OP_JT[instr](sys, cpu, addr_mode, instr, data, temp, addr);
        if (sys.cb_debug_post_exec) sys.cb_debug_post_exec(cpu);
        if (sys.cb_debug_instr_done) sys.cb_debug_instr_done(cpu);

        delta_ticks = ticks - start_ticks;
        if (delta_ticks == 0) {
            delta_ticks = 1;
        }

    }

    return true;
}
