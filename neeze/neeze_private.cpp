#include <cstdio>
#include "neeze_private.h"

void neeze::_log(const neeze::sys_t& sys, neeze::LOG_LEVEL level, const char* buffer, ...)
{
    static char _buffer[2048];
    static va_list va;

    if (!sys.cb_log)
        return;

    va_start(va, buffer);
    vsprintf(_buffer, buffer, va);
    va_end(va);

    sys.cb_log(level, _buffer);
}
