#ifndef NEEZE_PRIVATE_H
#define NEEZE_PRIVATE_H

#include <stdarg.h>
#include "neeze.h"

namespace neeze
{

// helper
void print_bits(uint8_t x);

// init functions
void init_ppu(sys_t& sys);
void init_cpu(sys_t& sys);

// step functions
uint32_t step_cpu(sys_t& sys, uint32_t max_ticks);
uint32_t step_ppu(sys_t& sys, uint32_t max_ticks);

void _log(const neeze::sys_t& sys, neeze::LOG_LEVEL level, const char* buffer, ...);
#ifdef DEBUG
    #define LOG_D(...) neeze::_log(sys, LOG_LEVEL_DEBUG, __VA_ARGS__)
#else
    #define LOG_D(...)
#endif
#define LOG_I(...) neeze::_log(sys, LOG_LEVEL_INFO, __VA_ARGS__)
#define LOG_W(...) neeze::_log(sys, LOG_LEVEL_WARNING, __VA_ARGS__)
#define LOG_E(...) neeze::_log(sys, LOG_LEVEL_ERROR, __VA_ARGS__)

#define SET_NEGATIVE(n)     cpu.reg_P = ((cpu.reg_P & (~0x80)) | (n << 7)) //cpu.reg_P.S = n;
#define SET_OVERFLOW(n)     cpu.reg_P = ((cpu.reg_P & (~0x40)) | (n << 6)) //cpu.reg_P.V = n;

#define SET_BREAK(n)        cpu.reg_P = ((cpu.reg_P & (~0x10)) | (n << 4)) //cpu.reg_P.B = n;
#define SET_DECIMAL(n)      cpu.reg_P = ((cpu.reg_P & (~0x08)) | (n << 3)) //cpu.reg_P.D = n;
#define SET_INTERRUPT(n)    cpu.reg_P = ((cpu.reg_P & (~0x04)) | (n << 2)) //cpu.reg_P.I = n;
#define SET_ZERO(n)         cpu.reg_P = ((cpu.reg_P & (~0x02)) | (n << 1)) //cpu.reg_P.Z = n;
#define SET_CARRY(n)        cpu.reg_P = ((cpu.reg_P & (~0x01)) | (n << 0)) //cpu.reg_P.C = n;


#define GET_NEGATIVE        !!(cpu.reg_P & (0x80)) //cpu.reg_P.S
#define GET_OVERFLOW        !!(cpu.reg_P & (0x40)) //cpu.reg_P.V

#define GET_BREAK           !!(cpu.reg_P & (0x10)) //cpu.reg_P.B
#define GET_DECIMAL         !!(cpu.reg_P & (0x08)) //cpu.reg_P.D
#define GET_INTERRUPT       !!(cpu.reg_P & (0x04)) //cpu.reg_P.I
#define GET_ZERO            !!(cpu.reg_P & (0x02)) //cpu.reg_P.Z
#define GET_CARRY           !!(cpu.reg_P & (0x01)) //cpu.reg_P.C


#define CALC_CARRY(n)       cpu.reg_P = ((cpu.reg_P & (~0x01)) | ( ( (n > 0xFF) ? 0x1 : 0x0 ) ) ) //SET_CARRY( (n > 0xFF) ? 1 : 0 )
#define CALC_ZERO(n)        cpu.reg_P = ((cpu.reg_P & (~0x02)) | ( ( (n & 0xFF) == 0x0) ? 0x02 : 0x0 )) //SET_ZERO( n == 0x0 )
#define CALC_OVERFLOW(a, b, c)  cpu.reg_P = ((cpu.reg_P & (~0x40)) | ( (!((a ^ b) & 0x80) && ((a ^ c) & 0x80)) ? 0x40 : 0x0 ) ) //SET_OVERFLOW( n > 0x00FF )
#define CALC_NEGATIVE(n)    cpu.reg_P = ((cpu.reg_P & (~0x80)) | ( ( (n & (1 << 7)) ? 0x80 : 0x0 ) ) ) //SET_NEGATIVE( n & (1 << 7) )

typedef uint8_t (nes_op_func_t)(sys_t& sys, cpu_t& cpu, NES_OP_ADDR_MODES addr_mode,
                             uint8_t instr, uint16_t& data, uint16_t& temp, uint16_t& addr);

#define NES_OP_FUNC_IMPL(name) \
    uint8_t neeze::op_##name(sys_t& sys, cpu_t& cpu, NES_OP_ADDR_MODES addr_mode, \
                             uint8_t instr, uint16_t& data, uint16_t& temp, uint16_t& addr)
#define NES_OP_FUNC(name) \
    uint8_t op_##name(sys_t& sys, cpu_t& cpu, NES_OP_ADDR_MODES addr_mode, \
                      uint8_t instr, uint16_t& data, uint16_t& temp, uint16_t& addr);

NES_OP_FUNC(ADC);
NES_OP_FUNC(AND);
NES_OP_FUNC(ASL);
NES_OP_FUNC(BCC);
NES_OP_FUNC(BCS);
NES_OP_FUNC(BEQ);
NES_OP_FUNC(BIT);
NES_OP_FUNC(BMI);
NES_OP_FUNC(BNE);
NES_OP_FUNC(BPL);
NES_OP_FUNC(BRK);
NES_OP_FUNC(BVC);
NES_OP_FUNC(BVS);
NES_OP_FUNC(CLC);
NES_OP_FUNC(CLD);
NES_OP_FUNC(CLI);
NES_OP_FUNC(CLV);
NES_OP_FUNC(CMP);
NES_OP_FUNC(CPX);
NES_OP_FUNC(CPY);
NES_OP_FUNC(DEC);
NES_OP_FUNC(DEX);
NES_OP_FUNC(DEY);
NES_OP_FUNC(EOR);
NES_OP_FUNC(INC);
NES_OP_FUNC(INX);
NES_OP_FUNC(INY);
NES_OP_FUNC(JMP);
NES_OP_FUNC(JSR);
NES_OP_FUNC(LDA);
NES_OP_FUNC(LDX);
NES_OP_FUNC(LDY);
NES_OP_FUNC(LSR);
NES_OP_FUNC(NOP);
NES_OP_FUNC(ORA);
NES_OP_FUNC(PHA);
NES_OP_FUNC(PHP);
NES_OP_FUNC(PLA);
NES_OP_FUNC(PLP);
NES_OP_FUNC(ROL);
NES_OP_FUNC(ROR);
NES_OP_FUNC(RTI);
NES_OP_FUNC(RTS);
NES_OP_FUNC(SBC);
NES_OP_FUNC(SEC);
NES_OP_FUNC(SED);
NES_OP_FUNC(SEI);
NES_OP_FUNC(STA);
NES_OP_FUNC(STX);
NES_OP_FUNC(STY);
NES_OP_FUNC(TAX);
NES_OP_FUNC(TAY);
NES_OP_FUNC(TSX);
NES_OP_FUNC(TXA);
NES_OP_FUNC(TXS);
NES_OP_FUNC(TYA);
NES_OP_FUNC(UFO_NOP);
NES_OP_FUNC(UFO_LAX);
NES_OP_FUNC(UFO_SAX);
NES_OP_FUNC(UFO_SBC);
NES_OP_FUNC(UFO_DCP);
NES_OP_FUNC(UFO_ISB);
NES_OP_FUNC(UFO_SLO);
NES_OP_FUNC(UFO_RLA);
NES_OP_FUNC(UFO_SRE);
NES_OP_FUNC(UFO_RRA);
NES_OP_FUNC(INVALID);



enum NES_OPS {
    ADC_const = 0x69,
    ADC_zp    = 0x65,
    ADC_zp_x  = 0x75,
    ADC_abs   = 0x6D,
    ADC_abs_x = 0x7D,
    ADC_abs_y = 0x79,
    ADC_ind_x = 0x61,
    ADC_ind_y = 0x71,

    AND_const = 0x29,
    AND_zp    = 0x25,
    AND_zp_x  = 0x35,
    AND_abs   = 0x2D,
    AND_abs_x = 0x3D,
    AND_abs_y = 0x39,
    AND_ind_x = 0x21,
    AND_ind_y = 0x31,

    ASL_a     = 0x0A,
    ASL_zp    = 0x06,
    ASL_zp_x  = 0x16,
    ASL_abs   = 0x0E,
    ASL_abs_x = 0x1E,

    BCC       = 0x90,
    BCS       = 0xB0,
    BEQ       = 0xF0,
    BIT_zp    = 0x24,
    BIT_abs   = 0x2C,
    BMI       = 0x30,
    BNE       = 0xD0,
    BPL       = 0x10,
    BRK       = 0x00,
    BVC       = 0x50,
    BVS       = 0x70,

    CLC       = 0x18,
    CLD       = 0xD8,
    CLI       = 0x58,
    CLV       = 0xB8,

    CMP_const = 0xC9,
    CMP_zp    = 0xC5,
    CMP_zp_x  = 0xD5,
    CMP_abs   = 0xCD,
    CMP_abs_x = 0xDD,
    CMP_abs_y = 0xD9,
    CMP_ind_x = 0xC1,
    CMP_ind_y = 0xD1,

    CPX_const = 0xE0,
    CPX_zp    = 0xE4,
    CPX_abs   = 0xEC,

    CPY_const = 0xC0,
    CPY_zp    = 0xC4,
    CPY_abs   = 0xCC,

    DEC_zp    = 0xC6,
    DEC_zp_x  = 0xD6,
    DEC_abs   = 0xCE,
    DEC_abs_x = 0xDE,
    DEX       = 0xCA,
    DEY       = 0x88,

    EOR_const = 0x49,
    EOR_zp    = 0x45,
    EOR_zp_x  = 0x55,
    EOR_abs   = 0x4D,
    EOR_abs_x = 0x5D,
    EOR_abs_y = 0x59,
    EOR_ind_x = 0x41,
    EOR_ind_y = 0x51,

    INC_zp    = 0xE6,
    INC_zp_x  = 0xF6,
    INC_abs   = 0xEE,
    INC_abs_x = 0xFE,
    INX       = 0xE8,
    INY       = 0xC8,

    JMP_abs   = 0x4C,
    JMP_ind   = 0x6C,
    JSR       = 0x20,

    LDA_const = 0xA9,
    LDA_zp    = 0xA5,
    LDA_zp_x  = 0xB5,
    LDA_abs   = 0xAD,
    LDA_abs_x = 0xBD,
    LDA_abs_y = 0xB9,
    LDA_ind_x = 0xA1,
    LDA_ind_y = 0xB1,

    LDX_const = 0xA2,
    LDX_zp    = 0xA6,
    LDX_zp_y  = 0xB6,
    LDX_abs   = 0xAE,
    LDX_abs_y = 0xBE,

    LDY_const = 0xA0,
    LDY_zp    = 0xA4,
    LDY_zp_x  = 0xB4,
    LDY_abs   = 0xAC,
    LDY_abs_x = 0xBC,

    LSR_a     = 0x4A,
    LSR_zp    = 0x46,
    LSR_zp_x  = 0x56,
    LSR_abs   = 0x4E,
    LSR_abs_x = 0x5E,

    NOP       = 0xEA,

    ORA_const = 0x09,
    ORA_zp    = 0x05,
    ORA_zp_x  = 0x15,
    ORA_abs   = 0x0D,
    ORA_abs_x = 0x1D,
    ORA_abs_y = 0x19,
    ORA_ind_x = 0x01,
    ORA_ind_y = 0x11,

    PHA       = 0x48,
    PHP       = 0x08,
    PLA       = 0x68,
    PLP       = 0x28,

    ROL_a     = 0x2A,
    ROL_zp    = 0x26,
    ROL_zp_x  = 0x36,
    ROL_abs   = 0x2E,
    ROL_abs_x = 0x3E,

    ROR_a     = 0x6A,
    ROR_zp    = 0x66,
    ROR_zp_x  = 0x76,
    ROR_abs   = 0x6E,
    ROR_abs_x = 0x7E,

    RTI       = 0x40,
    RTS       = 0x60,

    SBC_const = 0xE9,
    SBC_zp    = 0xE5,
    SBC_zp_x  = 0xF5,
    SBC_abs   = 0xED,
    SBC_abs_x = 0xFD,
    SBC_abs_y = 0xF9,
    SBC_ind_x = 0xE1,
    SBC_ind_y = 0xF1,

    SEC       = 0x38,
    SED       = 0xF8,
    SEI       = 0x78,

    STA_zp    = 0x85,
    STA_zp_x  = 0x95,
    STA_abs   = 0x8D,
    STA_abs_x = 0x9D,
    STA_abs_y = 0x99,
    STA_ind_x = 0x81,
    STA_ind_y = 0x91,

    STX_zp    = 0x86,
    STX_zp_y  = 0x96,
    STX_abs   = 0x8E,

    STY_zp    = 0x84,
    STY_zp_x  = 0x94,
    STY_abs   = 0x8C,

    TAX       = 0xAA,
    TAY       = 0xA8,
    TSX       = 0xBA,
    TXA       = 0x8A,
    TXS       = 0x9A,
    TYA       = 0x98,

    // Unofficial opcodes
    // Docs:
    // - http://www.oxyron.de/html/opcodes02.html
    // - https://wiki.nesdev.com/w/index.php/Programming_with_unofficial_opcodes
    // - https://wiki.nesdev.com/w/index.php/CPU_unofficial_opcodes
    // - http://nesdev.com/6502_cpu.txt
    // - http://visual6502.org/wiki/index.php?title=6502_all_256_Opcodes
    UFO_NOP_04 = 0x04,
    UFO_NOP_44 = 0x44,
    UFO_NOP_64 = 0x64,
    UFO_NOP_0C = 0x0C,
    UFO_NOP_14 = 0x14,
    UFO_NOP_34 = 0x34,
    UFO_NOP_54 = 0x54,
    UFO_NOP_74 = 0x74,
    UFO_NOP_D4 = 0xD4,
    UFO_NOP_F4 = 0xF4,
    UFO_NOP_1A = 0x1A,
    UFO_NOP_3A = 0x3A,
    UFO_NOP_5A = 0x5A,
    UFO_NOP_7A = 0x7A,
    UFO_NOP_DA = 0xDA,
    UFO_NOP_FA = 0xFA,
    UFO_NOP_80 = 0x80,
    UFO_NOP_1C = 0x1C,
    UFO_NOP_3C = 0x3C,
    UFO_NOP_5C = 0x5C,
    UFO_NOP_7C = 0x7C,
    UFO_NOP_DC = 0xDC,
    UFO_NOP_FC = 0xFC,

    UFO_LAX_A3_ind_x = 0xA3,
    UFO_LAX_A7_zp    = 0xA7,
    UFO_LAX_AF_abs   = 0xAF,
    UFO_LAX_B3_ind_y = 0xB3,
    UFO_LAX_B7_zp_x  = 0xB7,
    UFO_LAX_BF_abs_y = 0xBF,

    UFO_SAX_83_ind_x = 0x83,
    UFO_SAX_87_zp    = 0x87,
    UFO_SAX_8F_abs   = 0x8F,
    UFO_SAX_97_zp_x  = 0x97,

    UFO_SBC_EB_const = 0xEB,

    UFO_DCP_C3_ind_x = 0xC3,
    UFO_DCP_C7_zp    = 0xC7,
    UFO_DCP_CF_abs   = 0xCF,
    UFO_DCP_D3_ind_y = 0xD3,
    UFO_DCP_D7_zp_x  = 0xD7,
    UFO_DCP_DB_abs_y = 0xDB,
    UFO_DCP_DF_abs_x = 0xDF,

    UFO_ISB_E3_ind_x = 0xE3,
    UFO_ISB_E7_zp    = 0xE7,
    UFO_ISB_EF_abs   = 0xEF,
    UFO_ISB_F3_ind_y = 0xF3,
    UFO_ISB_F7_zp_x  = 0xF7,
    UFO_ISB_FB_abs_y = 0xFB,
    UFO_ISB_FF_abs_x = 0xFF,

    UFO_SLO_03_ind_x = 0x03,
    UFO_SLO_07_zp    = 0x07,
    UFO_SLO_0F_abs   = 0x0F,
    UFO_SLO_13_ind_y = 0x13,
    UFO_SLO_17_zp_x  = 0x17,
    UFO_SLO_1B_abs_y = 0x1B,
    UFO_SLO_1F_abs_x = 0x1F,

    UFO_RLA_23_ind_x = 0x23,
    UFO_RLA_27_zp    = 0x27,
    UFO_RLA_2F_abs   = 0x2F,
    UFO_RLA_33_ind_y = 0x33,
    UFO_RLA_37_zp_x  = 0x37,
    UFO_RLA_3B_abs_y = 0x3B,
    UFO_RLA_3F_abs_x = 0x3F,

    UFO_SRE_43_ind_x = 0x43,
    UFO_SRE_47_zp    = 0x47,
    UFO_SRE_4F_abs   = 0x4F,
    UFO_SRE_53_ind_y = 0x53,
    UFO_SRE_57_zp_x  = 0x57,
    UFO_SRE_5B_abs_y = 0x5B,
    UFO_SRE_5F_abs_x = 0x5F,

    UFO_RRA_63_ind_x = 0x63,
    UFO_RRA_67_zp    = 0x67,
    UFO_RRA_6F_abs   = 0x6F,
    UFO_RRA_73_ind_y = 0x73,
    UFO_RRA_77_zp_x  = 0x77,
    UFO_RRA_7B_abs_y = 0x7B,
    UFO_RRA_7F_abs_x = 0x7F,

};

static nes_op_func_t* NES_OP_JT[] = {
    op_BRK,
    op_ORA,
    op_INVALID,
    op_UFO_SLO,
    op_UFO_NOP,
    op_ORA,
    op_ASL,
    op_UFO_SLO,
    op_PHP,
    op_ORA,
    op_ASL,
    op_INVALID,
    op_UFO_NOP,
    op_ORA,
    op_ASL,
    op_UFO_SLO,
    op_BPL,
    op_ORA,
    op_INVALID,
    op_UFO_SLO,
    op_UFO_NOP,
    op_ORA,
    op_ASL,
    op_UFO_SLO,
    op_CLC,
    op_ORA,
    op_UFO_NOP,
    op_UFO_SLO,
    op_UFO_NOP,
    op_ORA,
    op_ASL,
    op_UFO_SLO,
    op_JSR,
    op_AND,
    op_INVALID,
    op_UFO_RLA,
    op_BIT,
    op_AND,
    op_ROL,
    op_UFO_RLA,
    op_PLP,
    op_AND,
    op_ROL,
    op_INVALID,
    op_BIT,
    op_AND,
    op_ROL,
    op_UFO_RLA,
    op_BMI,
    op_AND,
    op_INVALID,
    op_UFO_RLA,
    op_UFO_NOP,
    op_AND,
    op_ROL,
    op_UFO_RLA,
    op_SEC,
    op_AND,
    op_UFO_NOP,
    op_UFO_RLA,
    op_UFO_NOP,
    op_AND,
    op_ROL,
    op_UFO_RLA,
    op_RTI,
    op_EOR,
    op_INVALID,
    op_UFO_SRE,
    op_UFO_NOP,
    op_EOR,
    op_LSR,
    op_UFO_SRE,
    op_PHA,
    op_EOR,
    op_LSR,
    op_INVALID,
    op_JMP,
    op_EOR,
    op_LSR,
    op_UFO_SRE,
    op_BVC,
    op_EOR,
    op_INVALID,
    op_UFO_SRE,
    op_UFO_NOP,
    op_EOR,
    op_LSR,
    op_UFO_SRE,
    op_CLI,
    op_EOR,
    op_UFO_NOP,
    op_UFO_SRE,
    op_UFO_NOP,
    op_EOR,
    op_LSR,
    op_UFO_SRE,
    op_RTS,
    op_ADC,
    op_INVALID,
    op_UFO_RRA,
    op_UFO_NOP,
    op_ADC,
    op_ROR,
    op_UFO_RRA,
    op_PLA,
    op_ADC,
    op_ROR,
    op_INVALID,
    op_JMP,
    op_ADC,
    op_ROR,
    op_UFO_RRA,
    op_BVS,
    op_ADC,
    op_INVALID,
    op_UFO_RRA,
    op_UFO_NOP,
    op_ADC,
    op_ROR,
    op_UFO_RRA,
    op_SEI,
    op_ADC,
    op_UFO_NOP,
    op_UFO_RRA,
    op_UFO_NOP,
    op_ADC,
    op_ROR,
    op_UFO_RRA,
    op_UFO_NOP,
    op_STA,
    op_INVALID,
    op_UFO_SAX,
    op_STY,
    op_STA,
    op_STX,
    op_UFO_SAX,
    op_DEY,
    op_INVALID,
    op_TXA,
    op_INVALID,
    op_STY,
    op_STA,
    op_STX,
    op_UFO_SAX,
    op_BCC,
    op_STA,
    op_INVALID,
    op_INVALID,
    op_STY,
    op_STA,
    op_STX,
    op_UFO_SAX,
    op_TYA,
    op_STA,
    op_TXS,
    op_INVALID,
    op_INVALID,
    op_STA,
    op_INVALID,
    op_INVALID,
    op_LDY,
    op_LDA,
    op_LDX,
    op_UFO_LAX,
    op_LDY,
    op_LDA,
    op_LDX,
    op_UFO_LAX,
    op_TAY,
    op_LDA,
    op_TAX,
    op_INVALID,
    op_LDY,
    op_LDA,
    op_LDX,
    op_UFO_LAX,
    op_BCS,
    op_LDA,
    op_INVALID,
    op_UFO_LAX,
    op_LDY,
    op_LDA,
    op_LDX,
    op_UFO_LAX,
    op_CLV,
    op_LDA,
    op_TSX,
    op_INVALID,
    op_LDY,
    op_LDA,
    op_LDX,
    op_UFO_LAX,
    op_CPY,
    op_CMP,
    op_INVALID,
    op_UFO_DCP,
    op_CPY,
    op_CMP,
    op_DEC,
    op_UFO_DCP,
    op_INY,
    op_CMP,
    op_DEX,
    op_INVALID,
    op_CPY,
    op_CMP,
    op_DEC,
    op_UFO_DCP,
    op_BNE,
    op_CMP,
    op_INVALID,
    op_UFO_DCP,
    op_UFO_NOP,
    op_CMP,
    op_DEC,
    op_UFO_DCP,
    op_CLD,
    op_CMP,
    op_UFO_NOP,
    op_UFO_DCP,
    op_UFO_NOP,
    op_CMP,
    op_DEC,
    op_UFO_DCP,
    op_CPX,
    op_SBC,
    op_INVALID,
    op_UFO_ISB,
    op_CPX,
    op_SBC,
    op_INC,
    op_UFO_ISB,
    op_INX,
    op_SBC,
    op_NOP,
    op_UFO_SBC,
    op_CPX,
    op_SBC,
    op_INC,
    op_UFO_ISB,
    op_BEQ,
    op_SBC,
    op_INVALID,
    op_UFO_ISB,
    op_UFO_NOP,
    op_SBC,
    op_INC,
    op_UFO_ISB,
    op_SED,
    op_SBC,
    op_UFO_NOP,
    op_UFO_ISB,
    op_UFO_NOP,
    op_SBC,
    op_INC,
    op_UFO_ISB,
};

static neeze::NES_OP_ADDR_MODES NES_OP_ADDR_MODE_LUT[] = {
    neeze::No_Address,               //   1
    neeze::PreIndex_Indirect_X,      //   2
    neeze::Unused,                   //   3
    neeze::PreIndex_Indirect_X,      //   4
    neeze::Absolute_ZP,              //   5
    neeze::Absolute_ZP,              //   6
    neeze::Absolute_ZP,              //   7
    neeze::Absolute_ZP,              //   8
    neeze::No_Address,               //   9
    neeze::Const,                    //  10
    neeze::Accumulator,              //  11
    neeze::Unused,                   //  12
    neeze::Absolute,                 //  13
    neeze::Absolute,                 //  14
    neeze::Absolute,                 //  15
    neeze::Absolute,                 //  16
    neeze::Relative,                 //  17
    neeze::PostIndex_Indirect_Y,     //  18
    neeze::Unused,                   //  19
    neeze::PostIndex_Indirect_Y,     //  20
    neeze::Index_ZP_X,               //  21
    neeze::Index_ZP_X,               //  22
    neeze::Index_ZP_X,               //  23
    neeze::Index_ZP_X,               //  24
    neeze::No_Address,               //  25
    neeze::Index_Y,                  //  26
    neeze::No_Address,               //  27
    neeze::Index_Y,                  //  28
    neeze::Index_X,                  //  29
    neeze::Index_X,                  //  30
    neeze::Index_X,                  //  31
    neeze::Index_X,                  //  32
    neeze::Absolute,                 //  33
    neeze::PreIndex_Indirect_X,      //  34
    neeze::Unused,                   //  35
    neeze::PreIndex_Indirect_X,      //  36
    neeze::Absolute_ZP,              //  37
    neeze::Absolute_ZP,              //  38
    neeze::Absolute_ZP,              //  39
    neeze::Absolute_ZP,              //  40
    neeze::No_Address,               //  41
    neeze::Const,                    //  42
    neeze::Accumulator,              //  43
    neeze::Unused,                   //  44
    neeze::Absolute,                 //  45
    neeze::Absolute,                 //  46
    neeze::Absolute,                 //  47
    neeze::Absolute,                 //  48
    neeze::Relative,                 //  49
    neeze::PostIndex_Indirect_Y,     //  50
    neeze::Unused,                   //  51
    neeze::PostIndex_Indirect_Y,     //  52
    neeze::Index_ZP_X,               //  53
    neeze::Index_ZP_X,               //  54
    neeze::Index_ZP_X,               //  55
    neeze::Index_ZP_X,               //  56
    neeze::No_Address,               //  57
    neeze::Index_Y,                  //  58
    neeze::No_Address,               //  59
    neeze::Index_Y,                  //  60
    neeze::Index_X,                  //  61
    neeze::Index_X,                  //  62
    neeze::Index_X,                  //  63
    neeze::Index_X,                  //  64
    neeze::No_Address,               //  65
    neeze::PreIndex_Indirect_X,      //  66
    neeze::Unused,                   //  67
    neeze::PreIndex_Indirect_X,      //  68
    neeze::Absolute_ZP,              //  69
    neeze::Absolute_ZP,              //  70
    neeze::Absolute_ZP,              //  71
    neeze::Absolute_ZP,              //  72
    neeze::No_Address,               //  73
    neeze::Const,                    //  74
    neeze::Accumulator,              //  75
    neeze::Unused,                   //  76
    neeze::Absolute,                 //  77
    neeze::Absolute,                 //  78
    neeze::Absolute,                 //  79
    neeze::Absolute,                 //  80
    neeze::Relative,                 //  81
    neeze::PostIndex_Indirect_Y,     //  82
    neeze::Unused,                   //  83
    neeze::PostIndex_Indirect_Y,     //  84
    neeze::Index_ZP_X,               //  85
    neeze::Index_ZP_X,               //  86
    neeze::Index_ZP_X,               //  87
    neeze::Index_ZP_X,               //  88
    neeze::No_Address,               //  89
    neeze::Index_Y,                  //  90
    neeze::No_Address,               //  91
    neeze::Index_Y,                  //  92
    neeze::Index_X,                  //  93
    neeze::Index_X,                  //  94
    neeze::Index_X,                  //  95
    neeze::Index_X,                  //  96
    neeze::No_Address,               //  97
    neeze::PreIndex_Indirect_X,      //  98
    neeze::Unused,                   //  99
    neeze::PreIndex_Indirect_X,      // 100
    neeze::Absolute_ZP,              // 101
    neeze::Absolute_ZP,              // 102
    neeze::Absolute_ZP,              // 103
    neeze::Absolute_ZP,              // 104
    neeze::No_Address,               // 105
    neeze::Const,                    // 106
    neeze::Accumulator,              // 107
    neeze::Unused,                   // 108
    neeze::Indirect,                 // 109
    neeze::Absolute,                 // 110
    neeze::Absolute,                 // 111
    neeze::Absolute,                 // 112
    neeze::Relative,                 // 113
    neeze::PostIndex_Indirect_Y,     // 114
    neeze::Unused,                   // 115
    neeze::PostIndex_Indirect_Y,     // 116
    neeze::Index_ZP_X,               // 117
    neeze::Index_ZP_X,               // 118
    neeze::Index_ZP_X,               // 119
    neeze::Index_ZP_X,               // 120
    neeze::No_Address,               // 121
    neeze::Index_Y,                  // 122
    neeze::No_Address,               // 123
    neeze::Index_Y,                  // 124
    neeze::Index_X,                  // 125
    neeze::Index_X,                  // 126
    neeze::Index_X,                  // 127
    neeze::Index_X,                  // 128
    neeze::Const,                    // 129
    neeze::PreIndex_Indirect_X,      // 130
    neeze::Unused,                   // 131
    neeze::PreIndex_Indirect_X,      // 132
    neeze::Absolute_ZP,              // 133
    neeze::Absolute_ZP,              // 134
    neeze::Absolute_ZP,              // 135
    neeze::Absolute_ZP,              // 136
    neeze::No_Address,               // 137
    neeze::Unused,                   // 138
    neeze::No_Address,               // 139
    neeze::Unused,                   // 140
    neeze::Absolute,                 // 141
    neeze::Absolute,                 // 142
    neeze::Absolute,                 // 143
    neeze::Absolute,                 // 144
    neeze::Relative,                 // 145
    neeze::PostIndex_Indirect_Y,     // 146
    neeze::Unused,                   // 147
    neeze::Unused,                   // 148
    neeze::Index_ZP_X,               // 149
    neeze::Index_ZP_X,               // 150
    neeze::Index_ZP_Y,               // 151
    neeze::Index_ZP_Y,               // 152
    neeze::No_Address,               // 153
    neeze::Index_Y,                  // 154
    neeze::No_Address,               // 155
    neeze::Unused,                   // 156
    neeze::Unused,                   // 157
    neeze::Index_X,                  // 158
    neeze::Unused,                   // 159
    neeze::Unused,                   // 160
    neeze::Const,                    // 161
    neeze::PreIndex_Indirect_X,      // 162
    neeze::Const,                    // 163
    neeze::PreIndex_Indirect_X,      // 164
    neeze::Absolute_ZP,              // 165
    neeze::Absolute_ZP,              // 166
    neeze::Absolute_ZP,              // 167
    neeze::Absolute_ZP,              // 168
    neeze::No_Address,               // 169
    neeze::Const,                    // 170
    neeze::No_Address,               // 171
    neeze::Unused,                   // 172
    neeze::Absolute,                 // 173
    neeze::Absolute,                 // 174
    neeze::Absolute,                 // 175
    neeze::Absolute,                 // 176
    neeze::Relative,                 // 177
    neeze::PostIndex_Indirect_Y,     // 178
    neeze::Unused,                   // 179
    neeze::PostIndex_Indirect_Y,     // 180
    neeze::Index_ZP_X,               // 181
    neeze::Index_ZP_X,               // 182
    neeze::Index_ZP_Y,               // 183
    neeze::Index_ZP_Y,               // 184
    neeze::No_Address,               // 185
    neeze::Index_Y,                  // 186
    neeze::No_Address,               // 187
    neeze::Unused,                   // 188
    neeze::Index_X,                  // 189
    neeze::Index_X,                  // 190
    neeze::Index_Y,                  // 191
    neeze::Index_Y,                  // 192
    neeze::Const,                    // 193
    neeze::PreIndex_Indirect_X,      // 194
    neeze::Unused,                   // 195
    neeze::PreIndex_Indirect_X,      // 196
    neeze::Absolute_ZP,              // 197
    neeze::Absolute_ZP,              // 198
    neeze::Absolute_ZP,              // 199
    neeze::Absolute_ZP,              // 200
    neeze::No_Address,               // 201
    neeze::Const,                    // 202
    neeze::No_Address,               // 203
    neeze::Unused,                   // 204
    neeze::Absolute,                 // 205
    neeze::Absolute,                 // 206
    neeze::Absolute,                 // 207
    neeze::Absolute,                 // 208
    neeze::Relative,                 // 209
    neeze::PostIndex_Indirect_Y,     // 210
    neeze::Unused,                   // 211
    neeze::PostIndex_Indirect_Y,     // 212
    neeze::Index_ZP_X,               // 213
    neeze::Index_ZP_X,               // 214
    neeze::Index_ZP_X,               // 215
    neeze::Index_ZP_X,               // 216
    neeze::No_Address,               // 217
    neeze::Index_Y,                  // 218
    neeze::No_Address,               // 219
    neeze::Index_Y,                  // 220
    neeze::Index_X,                  // 221
    neeze::Index_X,                  // 222
    neeze::Index_X,                  // 223
    neeze::Index_X,                  // 224
    neeze::Const,                    // 225
    neeze::PreIndex_Indirect_X,      // 226
    neeze::Unused,                   // 227
    neeze::PreIndex_Indirect_X,      // 228
    neeze::Absolute_ZP,              // 229
    neeze::Absolute_ZP,              // 230
    neeze::Absolute_ZP,              // 231
    neeze::Absolute_ZP,              // 232
    neeze::No_Address,               // 233
    neeze::Const,                    // 234
    neeze::No_Address,               // 235
    neeze::Const,                    // 236
    neeze::Absolute,                 // 237
    neeze::Absolute,                 // 238
    neeze::Absolute,                 // 239
    neeze::Absolute,                 // 240
    neeze::Relative,                 // 241
    neeze::PostIndex_Indirect_Y,     // 242
    neeze::Unused,                   // 243
    neeze::PostIndex_Indirect_Y,     // 244
    neeze::Index_ZP_X,               // 245
    neeze::Index_ZP_X,               // 246
    neeze::Index_ZP_X,               // 247
    neeze::Index_ZP_X,               // 248
    neeze::No_Address,               // 249
    neeze::Index_Y,                  // 250
    neeze::No_Address,               // 251
    neeze::Index_Y,                  // 252
    neeze::Index_X,                  // 253
    neeze::Index_X,                  // 254
    neeze::Index_X,                  // 255
    neeze::Index_X,                  // 256
};

} // namespace neeze
#endif
