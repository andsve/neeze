#ifndef NEEZE_MEM_H
#define NEEZE_MEM_H

#include "neeze.h"

namespace neeze
{
    /*
        +---------------+ (0x10000)
        |    PRG ROM    |
        |  (upper bank) |          char* prgrom_upper;
        +---------------+ 0xC000
        |    PRG ROM    |
        |  (lower bank) |          char* prgrom_lower;
        +---------------+ 0x8000
        |      SRAM     |
        +---------------+ 0x6000
        | Expansion ROM |
        +---------------+ 0x4020
        | I/O Registers |
        +---------------+ 0x4000
        |    Mirrors    |
        | 0x2000-0x2007 |
        +---------------+ 0x0800
        |      RAM      |
        +---------------+ 0x0200
        |     Stack     |
        +---------------+ 0x0100
        |   Zero Page   |
        +---------------+ 0x0000

    */
    inline unsigned char memory_read(cpu_t &cpu, unsigned short addr)
    {
        // zero page
        if (addr < 0x0100)
        {
            return cpu.ram[addr];

        // stack
        } else if (addr < 0x0200) {

            return cpu.stack[addr - 0x0100];

        // non-zero page RAM
        // TODO Fix memory mirroring!
        } else if (addr < 0x4000) {

            return cpu.ram[addr - 0x0100];

        // i/o registers
        // TODO Fix I/O reg read and mirroring
        } else if (addr < 0x6000) {

            //return cpu.ram[addr - 0x0200];

        // Save RAM
        // TODO Fix save RAM read
        } else if (addr < 0x8000) {

            //return cpu.ram[addr - 0x0200];

        // PRG ROM lower
        } else if (addr < 0xC000) {

            //printf("getting lower prg ram %x -> %x, \n", addr, addr - 0x8000);
            return cpu.prgrom_lower[addr - 0x8000];

        // PRG ROM upper
        } else {
            //printf("getting upper prg ram %x -> %x, \n", addr, addr - 0xC000);
            return cpu.prgrom_upper[addr - 0xC000];
        }

        return 0x0;
    }

    inline unsigned short memory_read_short(cpu_t &cpu, unsigned short addr)
    {
        return ((unsigned short)memory_read(cpu, addr+1) << 8) | memory_read(cpu, addr);
    }

    inline unsigned short memory_read_short_zp_wrap(cpu_t &cpu, unsigned char addr)
    {
        unsigned char hi_addr = addr+1;
        unsigned char lo_addr = addr;
        return ((unsigned short)memory_read(cpu, hi_addr) << 8) | memory_read(cpu, lo_addr);
    }

    inline unsigned char memory_write(cpu_t &cpu, unsigned short addr, unsigned char data)
    {
        unsigned char prev = 0xff;

        //printf("Wrote %02X to %04X\n", data, addr);
        // zero page
        if (addr < 0x0100)
        {
            //printf("Writing to ZERO PAGE area.\n");
            prev = cpu.ram[addr];
            cpu.ram[addr] = data;

        // stack
        } else if (addr < 0x0200) {

            //printf("Writing to STACK area.\n");
            prev = cpu.stack[addr - 0x0100];
            cpu.stack[addr - 0x0100] = data;

        // non-zero page RAM
        // TODO Fix memory mirroring!
        } else if (addr < 0x4000) {

            prev = cpu.ram[addr - 0x0100];
            cpu.ram[addr - 0x0100] = data;

        // i/o registers
        // TODO Fix I/O reg read and mirroring
        } else if (addr < 0x6000) {

            //return cpu.ram[addr - 0x0200];
            cpu._4015 = data;

        // Save RAM
        // TODO Fix save RAM read
        } else if (addr < 0x8000) {

            //return cpu.ram[addr - 0x0200];
        }
        /*

        Should not be possible to write to ROM sections.

        // PRG ROM lower
        } else if (addr < 0xC000) {

            return cpu.prgrom_lower[addr - 0x8000];
        }

        // PRG ROM upper
        } else if (addr < 0x10000) {

            return cpu.prgrom_upper[addr - 0xC000];
        }

        */

        return prev;
    }

    inline void stack_push(cpu_t &cpu, unsigned char data)
    {
        //test_mem[cpu.reg_S] = data;
        //printf("Push to stack: %02X\n", data);
        memory_write(cpu, 0x0100 + cpu.reg_S, data);
        cpu.reg_S -= 0x1;
    }

    inline unsigned char stack_pop(cpu_t &cpu)
    {
        //unsigned char data = test_mem[cpu.reg_S];

        cpu.reg_S += 0x1;
        unsigned char data = memory_read(cpu, 0x0100 + cpu.reg_S);
        //printf("Pop from stack: %02X\n", data);

        return data;
    }

    inline void stack_push_short(cpu_t &cpu, unsigned short data)
    {
        stack_push(cpu, (0xFF00 & data) >> 8 );
        stack_push(cpu, 0x00FF & data );


        /*memory_write(cpu, 0x01FF - (cpu.reg_S), 0x00FF & data);
        //printf("[s%X]: %X\n", 0x01FF - (cpu.reg_S), 0x00FF & data);
        memory_write(cpu, 0x01FF - (cpu.reg_S+1), (0xFF00 & data) >> 8);
        //printf("[s%X]: %X\n", 0x01FF - (cpu.reg_S+1), (0xFF00 & data) >> 8);
        cpu.reg_S += 0x2;*/
    }

    inline unsigned short stack_pop_short(cpu_t &cpu)
    {

        //unsigned short data = ((unsigned short)memory_read(cpu, 0x0100 + (cpu.reg_S+1)) << 8) | memory_read(cpu, 0x0100 - (cpu.reg_S+2));
        //unsigned short data = memory_read_short(cpu, cpu.reg_S );
        unsigned short lo = (unsigned short)stack_pop(cpu);
        unsigned short hi = (unsigned short)stack_pop(cpu) << 8;
        unsigned short data = hi | lo;
        //cpu.reg_S += 0x2;

        //unsigned short data = memory_read_short(cpu, 0x01FF - (cpu.reg_S) );

        return data;
    }

    /*
    Indexed and Zero-page Indexed
    In these modes the address given is added to the value in either the X or
    Y index register to give the actual address of the operand.
    eg.  LDA $31F6, Y
       $D9 $31F6
       LDA $31F6, X
       $DD $31F6
    Note that the different operation codes determine the index register used.
    In the zero-page version, you should note that the X and Y registers are
    not interchangeable. Most instructions which can be used with zero-page
    indexing do so with X only.
    eg.  LDA $20, X
       $B5 $20

    */
    inline unsigned short addr_indexed( unsigned short addr, unsigned char index )
    {
        return addr + index;
    }

    /*
    Pre-indexed indirect
    In this mode a zer0-page address is added to the contents of the X-register
    to give the address of the bytes holding the address of the operand. The
    indirection is indicated by parenthesis in assembly language.
    eg.  LDA ($3E, X)
       $A1 $3E
    Assume the following -        byte      value
                                X-reg.    $05
                                $0043     $15
                                $0044     $24
                                $2415     $6E

    Then the instruction is executed by:
    (i)   adding $3E and $05 = $0043
    (ii)  getting address contained in bytes $0043, $0044 = $2415
    (iii) loading contents of $2415 - i.e. $6E - into accumulator
    */
    /*inline unsigned short addr_preindexed_indirect( cpu_t &cpu, unsigned short addr, unsigned char index )
    {
        return memory_read_short( cpu, addr + index );
    }*/
    inline unsigned short addr_preindexed_indirect_zeropage( cpu_t &cpu, unsigned char addr, unsigned char index )
    {
        addr += index;
        //unsigned short knug = memory_read_short_zp_wrap( cpu, addr );
        //printf("%04x %04x\n", addr, knug);
        //return knug;
        return memory_read_short_zp_wrap( cpu, addr );
    }

    /*
    Post-indexed indirect
    In this mode the contents of a zero-page address (and the following byte)
    give the indirect addressm which is added to the contents of the Y-register
    to yield the actual address of the operand. Again, inassembly language,
    the instruction is indicated by parenthesis.
    eg.  LDA ($4C), Y
    Note that the parenthesis are only around the 2nd byte of the instruction
    since it is the part that does the indirection.
    Assume the following -        byte       value
                                $004C      $00
                                $004D      $21
                                Y-reg.     $05
                                $2105      $6D
    Then the instruction above executes by:
    (i)   getting the address in bytes $4C, $4D = $2100
    (ii)  adding the contents of the Y-register = $2105
    (111) loading the contents of the byte $2105 - i.e. $6D into the
        accumulator.
    Note: only the Y-register is used in this mode.
    */
    inline unsigned short addr_postindexed_indirect( cpu_t &cpu, unsigned char addr, unsigned char index )
    {
        return memory_read_short_zp_wrap( cpu, addr ) + index;
    }

    /*
    Indirect
    This mode applies only to the JMP instruction - JuMP to new location. It is
    indicated by parenthesis around the operand. The operand is the address of
    the bytes whose value is the new location.
    eg.  JMP ($215F)
    Assume the following -        byte      value
                                $215F     $76
                                $2160     $30
    This instruction takes the value of bytes $215F, $2160 and uses that as the
    address to jump to - i.e. $3076 (remember that addresses are stored with
    low byte first).
    */
    inline unsigned short addr_indirect( cpu_t &cpu, unsigned short addr )
    {
        return memory_read_short( cpu, addr );
    }
}

#endif
