#include "neeze.h"
#include "neeze_private.h"
#include "neeze_mem.h"

#include <cstdio>
#include <stdlib.h>

using namespace neeze;

NES_OP_FUNC_IMPL(INVALID)
{
    LOG_E("Unknown OPCODE: %02X", instr);
    exit(1);
    return 0;
}

/////////////////////////////////////////////////////////
// ADC group
// A + M + C -> A, C
//
// N Z C I D V
// | | | _ _ |
//
NES_OP_FUNC_IMPL(ADC)
{
    if (addr_mode != neeze::Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    temp = data;
    data = cpu.reg_A + data + GET_CARRY;

    CALC_CARRY(data);
    CALC_ZERO(data);
    CALC_NEGATIVE(data);
    CALC_OVERFLOW(cpu.reg_A, temp, data);

    cpu.reg_A = data;

    return 0;
}

/////////////////////////////////////////////////////////
// AND group
// A /\ M -> A
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(AND)
{
    if (addr_mode != neeze::Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    data = cpu.reg_A & data;
    cpu.reg_A = data;

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// ASL group - Shift Left One Bit (Memory or Accumulator)
//
// Operation: C <- |7|6|5|4|3|2|1|0| <- 0
//
// N Z C I D V
// | | | _ _ _
//
NES_OP_FUNC_IMPL(ASL)
{
    if (addr_mode == neeze::Accumulator)
    {
        if (sys.cb_debug_reg_write) sys.cb_debug_reg_write(cpu, 'A');
        data = data << 1;
        cpu.reg_A = data;

    } else {
        if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
        data = data << 1;
        memory_write(cpu, addr, data);
    }

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    CALC_CARRY(data);
    return 0;
}

/////////////////////////////////////////////////////////
// BCC - Branch on Carry Clear
//
// Operation: Branch on C = 0
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BCC)
{
    if (!GET_CARRY) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// BCS - Branch on carry set
//
// Operation: Branch on C = 1
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BCS)
{
    if (GET_CARRY) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// BEQ - Branch on result zero
//
// Operation: Branch on Z = 1
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BEQ)
{
    if (GET_ZERO) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// BIT - Test bits in memory with accumulator
//
// Operation: A /\ M, M7 -> N, M6 -> V
//
// N Z C I D V
// M7| _ _ _ M6
//
// Bit 6 and 7 are transferred to the status register.
// If the result of A /\ M is zero then Z = 1, otherwise Z = 0
//
NES_OP_FUNC_IMPL(BIT)
{
    SET_NEGATIVE( ((data >> 7) & 0x1) );
    SET_OVERFLOW( ((data >> 6) & 0x1) );

    data = cpu.reg_A & data;
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
    CALC_ZERO(data);
    return 3;
}

/////////////////////////////////////////////////////////
// BMI - Branch on result minus
//
// Operation: Branch on N = 1
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BMI)
{
    if (GET_NEGATIVE) {
        cpu.reg_PC = addr;
        // branched = true;
    }
    return 0;
}

/////////////////////////////////////////////////////////
// BNE - Branch on result not zero
//
// Operation: Branch on Z = 0
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BNE)
{
    if (!GET_ZERO) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// BPL - Branch on result plus
//
// Operation: Branch on N = 0
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BPL)
{
    if (!GET_NEGATIVE) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// BRK - Force Break
//
// Operation: Forced Interrupt PC + 2 toS P toS
//
// N Z C I D V
// _ _ _ 1 _ _
//
NES_OP_FUNC_IMPL(BRK)
{
    // TEMP FIX
    stack_push_short(cpu, cpu.reg_PC+1);

    stack_push(cpu, cpu.reg_P | 0x10);

    //cpu.reg_P.I = 1;
    SET_INTERRUPT(1);
    return 0;
}

/////////////////////////////////////////////////////////
// BVC - Branch on overflow clear
//
// Operation: Branch on V = 0
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BVC)
{
    if (!GET_OVERFLOW) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// BVS - Branch on overflow set
//
// Operation: Branch on V = 1
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(BVS)
{
    if (GET_OVERFLOW) {
        cpu.reg_PC = addr;
        return 3;
    }
    return 2;
}

/////////////////////////////////////////////////////////
// CLC - Clear carry flag
//
// Operation: 0 -> C
//
// N Z C I D V
// _ _ 0 _ _ _
//
NES_OP_FUNC_IMPL(CLC)
{
    SET_CARRY(0);
    return 2;
}

/////////////////////////////////////////////////////////
// CLD - Clear decimal mode
//
// Operation: 0 -> D
//
// N Z C I D V
// _ _ _ _ 0 _
//
NES_OP_FUNC_IMPL(CLD)
{
    SET_DECIMAL(0);
    return 0;
}

/////////////////////////////////////////////////////////
// CLI - Clear interrupt disable bit
//
// Operation: 0 -> I
//
// N Z C I D V
// _ _ _ 0 _ _
//
NES_OP_FUNC_IMPL(CLI)
{
    SET_INTERRUPT(0);
    return 0;
}

/////////////////////////////////////////////////////////
// CLV - Clear overflow flag
//
// Operation: 0 -> V
//
// N Z C I D V
// _ _ _ _ _ 0
//
NES_OP_FUNC_IMPL(CLV)
{
    SET_OVERFLOW(0);
    return 0;
}

/////////////////////////////////////////////////////////
// CMP - Compare memory and accumulator
//
// Operation: A - M
//
// N Z C I D V
// | | | _ _ _
//
NES_OP_FUNC_IMPL(CMP)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    temp = data;
    data = cpu.reg_A - (data & 0xFF);

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    SET_CARRY( (cpu.reg_A >= temp) ? 1 : 0 );
    return 0;
}

/////////////////////////////////////////////////////////
// CPX - Compare Memory and Index X
//
// Operation: X - M
//
// N Z C I D V
// | | | _ _ _
//
NES_OP_FUNC_IMPL(CPX)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    temp = data;
    data = cpu.reg_X - data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    SET_CARRY( (cpu.reg_X >= temp) ? 1 : 0 );
    return 0;
}

/////////////////////////////////////////////////////////
// CPY - Compare memory and index Y
//
// Operation: Y - M
//
// N Z C I D V
// | | | _ _ _
//
NES_OP_FUNC_IMPL(CPY)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    temp = data;
    data = cpu.reg_Y - data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    SET_CARRY( (cpu.reg_Y >= temp) ? 1 : 0 );
    return 0;
}

/////////////////////////////////////////////////////////
// DEC - Decrement memory by one
//
// Operation: M - 1 -> M
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(DEC)
{
    data = data - 1;
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), data);
    memory_write(cpu, addr, data);
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// DEX - Decrement index X by one
//
// Operation: X - 1 -> X
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(DEX)
{
    data = cpu.reg_X - 1;
    cpu.reg_X = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// DEY - Decrement index Y by one
//
// Operation: Y - 1 -> Y
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(DEY)
{
    data = cpu.reg_Y - 1;
    cpu.reg_Y = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// EOR - "Exclusive-Or" memory with accumulator
//
// Operation: A EOR M -> A
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(EOR)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    data = cpu.reg_A ^ data;
    cpu.reg_A = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// INC - Increment memory by one
//
// Operation: M + 1 -> M
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(INC)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
    data = data + 1;
    memory_write(cpu, addr, data);
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// INX - Increment Index X by one
//
// Operation: X + 1 -> X
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(INX)
{
    data = cpu.reg_X + 1;
    cpu.reg_X = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// INY - Increment Index Y by one
//
// Operation: Y + 1 -> Y
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(INY)
{
    data = cpu.reg_Y + 1;
    cpu.reg_Y = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// JMP - Jump to new location
//
// Operation: (PC + 1) -> PCL
//            (PC + 2) -> PCH
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(JMP)
{
    cpu.reg_PC = addr;
    return 3;
}

/////////////////////////////////////////////////////////
// JSR - Jump to new location saving return address
//
// Operation: PC + 2 toS, (PC + 1) -> PCL
//                        (PC + 2) -> PCH
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(JSR)
{
    stack_push_short(cpu, cpu.reg_PC-1);
    cpu.reg_PC = addr;
    return 6;
}

/////////////////////////////////////////////////////////
// LDA - Load accumulator with memory
//
// Operation: M -> A
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(LDA)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    cpu.reg_A = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 2;
}


/////////////////////////////////////////////////////////
// LDX - Load index X with memory
//
// Operation: M -> X
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(LDX)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    cpu.reg_X = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 2;
}

/////////////////////////////////////////////////////////
// LDY - Load index Y with memory
//
// Operation: M -> Y
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(LDY)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    cpu.reg_Y = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// LSR  - Shift right one bit (memory or accumulator)
//
// Operation: 0 -> |7|6|5|4|3|2|1|0| -> C
//
// N Z C I D V
// 0 | | _ _ _
//
NES_OP_FUNC_IMPL(LSR)
{
    SET_CARRY(data & 0x1);

    if (addr_mode == Accumulator)
    {
        if (sys.cb_debug_reg_write) sys.cb_debug_reg_write(cpu, 'A');
        data = data >> 1;

        cpu.reg_A = data;
    } else {
        if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
        data = data >> 1;

        memory_write(cpu, addr, data);
    }
    SET_NEGATIVE(0);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// NOP - No operation
//
// Operation: No Operation (2 cycles)
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(NOP)
{
    // TODO Take care of cycles?
    return 2;
}

/////////////////////////////////////////////////////////
// ORA - "OR" memory with accumulator
//
// Operation: A V M -> A
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(ORA)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write)
            sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    data = cpu.reg_A | data;
    cpu.reg_A = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

/////////////////////////////////////////////////////////
// PHA - Push accumulator on stack
//
// Operation: A toS
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(PHA)
{
    stack_push(cpu, cpu.reg_A);
    return 0;
}

/////////////////////////////////////////////////////////
// PHP - Push processor status on stack
//
// Operation: P toS
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(PHP)
{
    stack_push(cpu, cpu.reg_P | 0x10);
    return 3;
}

/////////////////////////////////////////////////////////
// PLA - Pull accumulator from stack
//
// Operation: A fromS
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(PLA)
{
    cpu.reg_A = stack_pop(cpu);
    CALC_NEGATIVE(cpu.reg_A);
    CALC_ZERO(cpu.reg_A);
    return 4;
}

/////////////////////////////////////////////////////////
// PLP - Pull accumulator from stack
//
// Operation: P fromS
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(PLP)
{
    cpu.reg_P = (stack_pop(cpu) | 0x20) & 0xEF;
    return 4;
}

/////////////////////////////////////////////////////////
// ROL - Rotate one bit left (memory or accumulator)
//
//            +------------------------------+
//            |         M or A               |
//            |   +-+-+-+-+-+-+-+-+    +-+   |
// Operation: +-< |7|6|5|4|3|2|1|0| <- |C| <-+
//                +-+-+-+-+-+-+-+-+    +-+
//
// N Z C I D V
// | | | _ _ _
//
NES_OP_FUNC_IMPL(ROL)
{
    data = data << 1;
    data |= GET_CARRY;

    if (addr_mode == Accumulator) {
        if (sys.cb_debug_reg_write) sys.cb_debug_reg_write(cpu, 'A');
        cpu.reg_A = data;
    } else {
        if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
        memory_write(cpu, addr, data);
    }

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    CALC_CARRY(data);

    return 2;
}

/////////////////////////////////////////////////////////
// ROR - Rotate one bit right (memory or accumulator)
//
//            +------------------------------+
//            |         M or A               |
//            |   +-+    +-+-+-+-+-+-+-+-+   |
// Operation: +-> |C| -> |7|6|5|4|3|2|1|0| >-+
//                +-+    +-+-+-+-+-+-+-+-+
//
// N Z C I D V
// | | | _ _ _
//
NES_OP_FUNC_IMPL(ROR)
{
    temp = GET_CARRY;
    SET_CARRY(data & 0x1);
    data = data >> 1;
    data |= (temp << 7);

    if (addr_mode == Accumulator) {
        if (sys.cb_debug_reg_write) sys.cb_debug_reg_write(cpu, 'A');
        cpu.reg_A = data;
    } else {
        if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
        memory_write(cpu, addr, data);
    }

    CALC_NEGATIVE(data);
    CALC_ZERO(data);

    return 0;
}

/////////////////////////////////////////////////////////
// RTI - Return from interrupt
//
// Operation: P fromS, PC fromS
//
// N Z C I D V
// From Stack
//
NES_OP_FUNC_IMPL(RTI)
{
    cpu.reg_P = stack_pop(cpu) | 0x20;
    cpu.reg_PC = stack_pop_short(cpu);
    return 0;
}

/////////////////////////////////////////////////////////
// RTS - Return from subroutine
//
// Operation: PC fromS, PC + 1 -> PC
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(RTS)
{
    cpu.reg_PC = stack_pop_short(cpu);
    cpu.reg_PC += 0x1;
    return 6;
}

/////////////////////////////////////////////////////////
// SBC - Subtract memory from accumulator with borrow
//                    _
// Operation: A - M - C -> A
//       _
// Note: C = Borrow
//
// N Z C I D V
// | | | _ _ |
//
NES_OP_FUNC_IMPL(SBC)
{
    if (addr_mode != Const)
        if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    data ^= 0xFF;
    temp = data;
    data = cpu.reg_A + data + GET_CARRY;

    CALC_CARRY(data);
    CALC_ZERO(data);
    CALC_NEGATIVE(data);
    CALC_OVERFLOW(cpu.reg_A, temp, data);

    cpu.reg_A = data;
    return 0;
}

/////////////////////////////////////////////////////////
// SEC - Set carry flag
//
// Operation: 1 -> C
//
// N Z C I D V
// _ _ 1 _ _ _
//
NES_OP_FUNC_IMPL(SEC)
{
    SET_CARRY(1);
    return 2;
}

/////////////////////////////////////////////////////////
// SED - Set decimal mode
//
// Operation: 1 -> D
//
// N Z C I D V
// _ _ _ _ 1 _
//
NES_OP_FUNC_IMPL(SED)
{
    SET_DECIMAL(1);
    return 2;
}

/////////////////////////////////////////////////////////
// SEI - Set interrupt disable status
//
// Operation: 1 -> I
//
// N Z C I D V
// _ _ _ 1 _ _
//
NES_OP_FUNC_IMPL(SEI)
{
    SET_INTERRUPT(1);
    return 2;
}

/////////////////////////////////////////////////////////
// STA - Store accumulator in memory
//
// Operation: A -> M
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(STA)
{
    temp = memory_write(cpu, addr, cpu.reg_A);
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, temp, cpu.reg_A);

    return 3;
}

/////////////////////////////////////////////////////////
// STX - Store index X in memory
//
// Operation: X -> M
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(STX)
{
    temp = memory_write(cpu, addr, cpu.reg_X);
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, temp, cpu.reg_A);
    return 3;
}

/////////////////////////////////////////////////////////
// STY - Store index Y in memory
//
// Operation: Y -> M
//
// N Z C I D V
// _ _ _ _ _ _
//
NES_OP_FUNC_IMPL(STY)
{
    temp = memory_write(cpu, addr, cpu.reg_Y);
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, temp, 0x0);
    return 0;
}

/////////////////////////////////////////////////////////
// TAX - Transfer accumulator to index X
//
// Operation: A -> X
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(TAX)
{
    cpu.reg_X = cpu.reg_A;
    CALC_NEGATIVE(cpu.reg_X);
    CALC_ZERO(cpu.reg_X);
    return 0;
}

/////////////////////////////////////////////////////////
// TAY - Transfer accumulator to index Y
//
// Operation: A -> Y
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(TAY)
{
    cpu.reg_Y = cpu.reg_A;
    CALC_NEGATIVE(cpu.reg_Y);
    CALC_ZERO(cpu.reg_Y);
    return 0;
}

/////////////////////////////////////////////////////////
// TSX - Transfer stack pointer to index X
//
// Operation: S -> X
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(TSX)
{
    cpu.reg_X = cpu.reg_S;
    CALC_NEGATIVE(cpu.reg_X);
    CALC_ZERO(cpu.reg_X);
    return 0;
}

/////////////////////////////////////////////////////////
// TXA - Transfer index X to accumulator
//
// Operation: X -> A
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(TXA)
{
    cpu.reg_A = cpu.reg_X;
    CALC_NEGATIVE(cpu.reg_A);
    CALC_ZERO(cpu.reg_A);
    return 0;
}

/////////////////////////////////////////////////////////
// TXS - Transfer index X to stack pointer
//
// Operation: X -> S
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(TXS)
{
    cpu.reg_S = cpu.reg_X;
    return 0;
}

/////////////////////////////////////////////////////////
// TYA - Transfer index Y to accumulator
//
// Operation: Y -> A
//
// N Z C I D V
// | | _ _ _ _
//
NES_OP_FUNC_IMPL(TYA)
{
    cpu.reg_A = cpu.reg_Y;
    CALC_NEGATIVE(cpu.reg_A);
    CALC_ZERO(cpu.reg_A);
    return 0;
}

/////////////////////////////////////////////////////////
// Unoffical Opcodes (UFO_*)
//

NES_OP_FUNC_IMPL(UFO_NOP)
{
    // TODO calc cycles
    if (addr_mode != Const && addr_mode != No_Address)
        if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
    return 0;
}

// LAX
// Shortcut for LDA value then TAX. Saves a byte and two cycles
// and allows use of the X register with the (d),Y addressing mode.
// Notice that the immediate is missing; the opcode that would have
// been LAX is affected by line noise on the data bus.
// MOS 6502: even the bugs have bugs.
NES_OP_FUNC_IMPL(UFO_LAX)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
    cpu.reg_A = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    cpu.reg_X = cpu.reg_A;
    CALC_NEGATIVE(cpu.reg_X);
    CALC_ZERO(cpu.reg_X);
    return 0;
}

// SAX
// Stores the bitwise AND of A and X. As with STA and STX, no flags are affected.
NES_OP_FUNC_IMPL(UFO_SAX)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), cpu.reg_A);
    memory_write(cpu, addr, cpu.reg_A & cpu.reg_X);
    return 0;
}

// DCP
// Equivalent to DEC value then CMP value, except supporting more addressing modes.
// LDA #$FF followed by DCP can be used to check if the decrement underflows,
// which is useful for multi-byte decrements.
NES_OP_FUNC_IMPL(UFO_DCP)
{
    // DEC
    temp = data; // uncertain if this is correct
    data = data - 1;
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), data);
    memory_write(cpu, addr, data);
    CALC_NEGATIVE(data);
    CALC_ZERO(data);

    // CMP
    // temp = data; // uncertain if this is correct
    data = cpu.reg_A - (data & 0xFF);

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    SET_CARRY( (cpu.reg_A >= temp) ? 1 : 0 );
    return 0;
}

// SLO
// Equivalent to ASL value then ORA value, except supporting more addressing modes.
// LDA #0 followed by SLO is an efficient way to shift a variable while also loading it in A.
NES_OP_FUNC_IMPL(UFO_SLO)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    data = data << 1;
    memory_write(cpu, addr, data);

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    CALC_CARRY(data);

    data = cpu.reg_A | data;
    cpu.reg_A = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

// RLA
// Equivalent to ROL value then AND value, except supporting more addressing modes.
// LDA #$FF followed by RLA is an efficient way to rotate a variable while also loading it in A.
NES_OP_FUNC_IMPL(UFO_RLA)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
    data = data << 1;
    data |= GET_CARRY;
    memory_write(cpu, addr, data);

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    CALC_CARRY(data);

    data = cpu.reg_A & data;
    cpu.reg_A = data;

    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

// SRE
// Equivalent to LSR value then EOR value, except supporting more addressing modes.
// LDA #0 followed by SRE is an efficient way to shift a variable while also loading it in A.
NES_OP_FUNC_IMPL(UFO_SRE)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    SET_CARRY(data & 0x1);
    data = data >> 1;
    memory_write(cpu, addr, data);

    SET_NEGATIVE(0);
    CALC_ZERO(data);

    data = cpu.reg_A ^ data;
    cpu.reg_A = data;
    CALC_NEGATIVE(data);
    CALC_ZERO(data);
    return 0;
}

// RRA
// Equivalent to ROR value then ADC value, except supporting more addressing modes.
// Essentially this computes A + value / 2, where value is 9-bit and the division is rounded up.
NES_OP_FUNC_IMPL(UFO_RRA)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);

    temp = GET_CARRY;
    SET_CARRY(data & 0x1);
    data = data >> 1;
    data |= (temp << 7);
    memory_write(cpu, addr, data);
    CALC_NEGATIVE(data);
    CALC_ZERO(data);

    temp = data;
    data = cpu.reg_A + data + GET_CARRY;
    //cpu.reg_A = data;

    CALC_CARRY(data);
    CALC_ZERO(data);
    CALC_NEGATIVE(data);
    CALC_OVERFLOW(cpu.reg_A, temp, data);

    cpu.reg_A = data;
    return 0;
}

// ISB
// Equivalent to INC value then SBC value, except supporting more addressing modes.
NES_OP_FUNC_IMPL(UFO_ISB)
{
    if (sys.cb_debug_mem_write) sys.cb_debug_mem_write(cpu, addr, memory_read(cpu, addr), 0x0);
    data = data + 1;
    memory_write(cpu, addr, data);
    CALC_NEGATIVE(data);
    CALC_ZERO(data);

    op_UFO_SBC(sys, cpu, addr_mode, instr, data, temp, addr);

    return 0;
}

// SBC
// Duplicates of ADC
// (0x69 and 0xE9 are official; 0xEB is not.
//  These three opcodes are nearly equivalent, except that 0xE9 and 0xEB add 255-i instead of i.)
// http://forums.nesdev.com/viewtopic.php?p=19080#19080
NES_OP_FUNC_IMPL(UFO_SBC)
{
    data = data ^ 0xFF;
    temp = data;
    data = cpu.reg_A + data + GET_CARRY;

    CALC_CARRY(data);
    CALC_ZERO(data);
    CALC_NEGATIVE(data);
    CALC_OVERFLOW(cpu.reg_A, temp, data);

    cpu.reg_A = data;
    return 0;
}
