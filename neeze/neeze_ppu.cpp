#include "neeze.h"
#include "neeze_mem.h"
#include "neeze_private.h"

void neeze::init_ppu(neeze::sys_t &sys)
{

    // From: https://wiki.nesdev.com/w/index.php/PPU_power_up_state
    //
    // Register                At Power    After Reset
    // -----------------------------------------------
    // PPUCTRL ($2000)         0000 0000     0000 0000
    // PPUMASK ($2001)         0000 0000     0000 0000
    // PPUSTATUS ($2002)       +0+x xxxx     U??x xxxx
    // OAMADDR ($2003)               $00    unchanged1
    // $2005 / $2006 latch       cleared       cleared
    // PPUSCROLL ($2005)           $0000         $0000
    // PPUADDR ($2006)             $0000     unchanged
    // PPUDATA ($2007)               $00           $00
    //
    // ? = unknown, x = irrelevant, + = often set, U = unchanged

    sys.ppu.ctrl   = 0x0;
    sys.ppu.mask   = 0x0;
    sys.ppu.status = 0x0;

    sys.ppu.x = 0;
    sys.ppu.y = 0;
}

uint32_t neeze::step_ppu(sys_t& sys, uint32_t max_ticks)
{
    // Christ allmighty:
    // PPU draws one pixel each PPU cycle.

    // Notes, does nothing for initial 29658 cycles

    // ppu_t& ppu = sys.ppu;

    static unsigned int ticks = 0x0;
    unsigned int delta_ticks = 0x0;
    unsigned int start_ticks = ticks;

    while (max_ticks > delta_ticks)
    {
        // Check if "beam" is in vertical area of render
        if (sys.ppu.y < 240) {
            uint16_t p = sys.ppu.y * 256 + sys.ppu.x;
            // LOG_D("x: %d, y: %d, %d", sys.ppu.x, sys.ppu.y, p);
            uint8_t* r = &sys.ppu.fb[p*3];
            uint8_t* g = &sys.ppu.fb[p*3+1];
            uint8_t* b = &sys.ppu.fb[p*3+2];
            *r = 0xff;
            *g = 0x0;
            *b = 0x0;
        }

        // Advance rasterer state:
        //   On an NTSC version of the NES, there are 240 scanlines on the screen (although the top
        //   and bottom eight lines are cut off) and it takes an additional 3 scanlines worth of CPU cycles
        //   to enter V-Blank. The V-Blank period takes a further 20 scanlines worth before the next
        //   frame can be drawn.
        //
        //   The NTSC video signal is made up of 262 scanlines, and 20 of those are spent in vblank state.
        //   After the program has received an NMI, it has about 2270 cycles to update the palette,
        //   sprites, and nametables as necessary before rendering begins.
        sys.ppu.x += 1;
        if (sys.ppu.x > 256) {
            sys.ppu.x = 0;
            sys.ppu.y += 1;
            if (sys.ppu.y > 240 + 3 + 20) {
                sys.ppu.y = 0; // TODO VSYNC
            }
        }

        ticks += 1;
        delta_ticks = ticks - start_ticks;
    }

    return 0;
}

