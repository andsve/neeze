solution "neeze"
   configurations { "Debug", "Release" }
   location "build"
   targetdir "bin"
   debugdir "./runtime/"
   platforms { "x64" }

   -- PPU visualizer
   -- ============
   project "ppu_vis"
      language "C++"
      files { "tests/ppu_vis.mm", "tests/minifb.mm" }
      includedirs { "neeze/" }

      links { "neeze_core" }

      if os.get() == "windows" then

      elseif (os.get() == "macosx") then
        buildoptions { "-Wall",
                       "-Werror",
                       "-Wno-deprecated"
                       -- "-fobjc-arc"
                     }
        links { "Cocoa.framework" }

        postbuildcommands { "cp ../tests/color_test.nes ../bin/color_test.nes",
                            "cp ../tests/scanline.nes ../bin/scanline.nes"
                          }
      end

      configuration "Debug"
         kind "ConsoleApp"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         kind "WindowedApp"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- ROM tester
   -- ============
   project "nestest_check"
      language "C++"
      files { "tests/nestest_check.cpp" }
      includedirs { "neeze/" }

      links { "neeze_core" }

      if os.get() == "windows" then

      elseif (os.get() == "macosx") then
        buildoptions { "-Wall",
                       "-Werror"
                     }
        postbuildcommands { "cp ../tests/nestest.log ../bin/nestest.log",
                            "cp ../tests/nestest.nes ../bin/nestest.nes"
                          }
      end

      configuration "Debug"
         kind "ConsoleApp"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         kind "WindowedApp"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- neeze core lib
   -- ============
   project "neeze_core"
      targetname ("neeze_core_internal")
      kind "StaticLib"
      language "C++"
      files { "neeze/**.cpp" }

      if os.get() == "windows" then
      elseif (os.get() == "macosx") then
        buildoptions { "-Wall", "-Werror" }
      end

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

