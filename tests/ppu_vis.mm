#include "minifb.h"
#include "../neeze/neeze.h"
#include <cstdio>
#include <cstdlib>

#define CHR_WIDTH 64
#define CHR_HEIGHT 64

#define WIDTH 256
// #define HEIGHT 240
#define HEIGHT (240 + CHR_HEIGHT) // add some extra for chr bank
static unsigned int s_buffer[WIDTH * HEIGHT];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void tmp_cb_log(neeze::LOG_LEVEL level, const char* log)
{
    printf("[%s] %s\n", level == neeze::LOG_LEVEL_DEBUG ? "DEBUG" : "\x1B[0;30;41mERROR\x1B[0;;m", log);
}

int main()
{
    // clear s_buffer
    for (int i = 0; i < WIDTH * HEIGHT; i++)
    {
        s_buffer[i] = MFB_RGB(0x0, 0x0, 0x0);
    }

    neeze::sys_t sys;
    sys.cb_log = tmp_cb_log;
    // sys.cb_debug_fetch_instr = tmp_cb_debug_fetch_instr;
    // sys.cb_debug_addr_mode = tmp_cb_debug_addr_mode;
    // sys.cb_debug_reg_write = tmp_cb_debug_reg_write;
    // sys.cb_debug_mem_write = tmp_cb_debug_mem_write;
    // sys.cb_debug_mem_read = tmp_cb_debug_mem_read;
    // sys.cb_debug_pre_exec = tmp_cb_debug_pre_exec;
    // sys.cb_debug_post_exec = tmp_cb_debug_post_exec;
    // sys.cb_debug_instr_done = tmp_cb_debug_instr_done;

    neeze::ines_rom_t rom;
    neeze::init(sys);
    neeze::load_rom_file(sys, "nestest.nes", rom);

    if (!mfb_open("nes ppu", WIDTH, HEIGHT))
        return 0;

    int once = 0;
    for (;;)
    {
        int i, state;
        uint8_t* fb = sys.ppu.fb;

        int chr_i = 0;

        for (i = 0; i < WIDTH * HEIGHT; ++i)
        {
            int x = i % WIDTH;
            int y = i / WIDTH;
            if (y < 240) {
                s_buffer[i] = MFB_RGB(fb[i*3], fb[i*3+1], fb[i*3+2]);
            } else {
                if (x < CHR_WIDTH) {
                    int chr_x = chr_i % CHR_WIDTH;
                    int chr_y = chr_i / CHR_WIDTH;

                    int d_i = chr_y*CHR_WIDTH + chr_x;
                    char* chr_data = rom.chr_pages[0];
                    s_buffer[i] = MFB_RGB(chr_data[d_i], 0x0, 0x0);
                    chr_i += 1;
                }
                // int chr_x = chr_i % WIDTH;
                // int chr_y = chr_i / WIDTH;
                // int d_i = chr_i;

                // char* chr_data = rom.chr_pages[0];
                // s_buffer[i] = MFB_RGB(chr_data[d_i], 0x0, 0x0);
                // chr_i += 1;
            }
        }

        state = mfb_update(s_buffer);

        if (!once)
            neeze::step(sys, 128);
        once += 1;

        if (state < 0)
            break;
    }

    mfb_close();

    return 0;
}
