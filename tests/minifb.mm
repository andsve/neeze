#include <Cocoa/Cocoa.h>
#include <unistd.h>
#include "minifb.h"

@interface OSXWindowFrameView : NSView
{
}

@end

@implementation OSXWindowFrameView

extern void* g_updateBuffer;
extern int g_width;
extern int g_height;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSRect)resizeRect
{
    const CGFloat resizeBoxSize = 16.0;
    const CGFloat contentViewPadding = 5.5;

    NSRect contentViewRect = [[self window] contentRectForFrameRect:[[self window] frame]];
    NSRect resizeRect = NSMakeRect(
        NSMaxX(contentViewRect) + contentViewPadding,
        NSMinY(contentViewRect) - resizeBoxSize - contentViewPadding,
        resizeBoxSize,
        resizeBoxSize);

    return resizeRect;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)drawRect:(NSRect)rect
{
    (void)rect;

    if (!g_updateBuffer)
        return;

    CGContextRef context = (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];

    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, g_updateBuffer, g_width * g_height * 4, NULL);

    CGImageRef img = CGImageCreate(g_width, g_height, 8, 32, g_width * 4, space, kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder32Little,
                                   provider, NULL, false, kCGRenderingIntentDefault);

    CGColorSpaceRelease(space);
    CGDataProviderRelease(provider);

    CGContextDrawImage(context, CGRectMake(0, 0, g_width, g_height), img);

    CGImageRelease(img);
}

@end

@interface OSXWindow : NSWindow
{
    NSView* childContentView;
    @public bool closed;
}

@end

@implementation OSXWindow

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (id)initWithContentRect:(NSRect)contentRect
    styleMask:(NSWindowStyleMask)windowStyle
    backing:(NSBackingStoreType)bufferingType
    defer:(BOOL)deferCreation
{
    self = [super
        initWithContentRect:contentRect
        styleMask:windowStyle
        backing:bufferingType
        defer:deferCreation];
    if (self)
    {
        [self setOpaque:YES];
        [self setBackgroundColor:[NSColor clearColor]];

        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(mainWindowChanged:)
            name:NSWindowDidBecomeMainNotification
            object:self];

        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(mainWindowChanged:)
            name:NSWindowDidResignMainNotification
            object:self];

        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(willClose)
            name:NSWindowWillCloseNotification
            object:self];

        closed = false;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]
        removeObserver:self];
    [super dealloc];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setContentSize:(NSSize)newSize
{
    NSSize sizeDelta = newSize;
    NSSize childBoundsSize = [childContentView bounds].size;
    sizeDelta.width -= childBoundsSize.width;
    sizeDelta.height -= childBoundsSize.height;

    OSXWindowFrameView *frameView = [super contentView];
    NSSize newFrameSize = [frameView bounds].size;
    newFrameSize.width += sizeDelta.width;
    newFrameSize.height += sizeDelta.height;

    [super setContentSize:newFrameSize];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)mainWindowChanged:(NSNotification *)aNotification
{
    (void)aNotification;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setContentView:(NSView *)aView
{
    if ([childContentView isEqualTo:aView])
    {
        return;
    }

    NSRect bounds = [self frame];
    bounds.origin = NSZeroPoint;

    OSXWindowFrameView *frameView = [super contentView];
    if (!frameView)
    {
        frameView = [[[OSXWindowFrameView alloc] initWithFrame:bounds] autorelease];

        [super setContentView:frameView];
    }

    if (childContentView)
    {
        [childContentView removeFromSuperview];
    }
    childContentView = aView;
    [childContentView setFrame:[self contentRectForFrameRect:bounds]];
    [childContentView setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
    [frameView addSubview:childContentView];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSView *)contentView
{
    return childContentView;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)canBecomeKeyWindow
{
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)canBecomeMainWindow
{
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSRect)contentRectForFrameRect:(NSRect)windowFrame
{
    windowFrame.origin = NSZeroPoint;
    return NSInsetRect(windowFrame, 0, 0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSRect)frameRectForContentRect:(NSRect)windowContentRect styleMask:(NSWindowStyleMask)windowStyle
{
    (void)windowStyle;
    return NSInsetRect(windowContentRect, 0, 0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)willClose
{
    closed = true;
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void* g_updateBuffer = 0;
int g_width = 0;
int g_height = 0;
static OSXWindow* window_;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int mfb_open(const char* name, int width, int height)
{
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

    g_width = width;
    g_height = height;

    [NSApplication sharedApplication];
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];

    NSWindowStyleMask styles = NSWindowStyleMaskResizable | NSWindowStyleMaskClosable | NSWindowStyleMaskTitled;

    NSRect rectangle = NSMakeRect(0, 0, width, height);
    // window_ = [[OSXWindow alloc] initWithContentRect:rectangle styleMask:styles backing:NSBackingStoreBuffered defer:NO];
    NSRect frameRect = [NSWindow frameRectForContentRect:rectangle styleMask:styles];
    window_ = [[OSXWindow alloc] initWithContentRect:frameRect styleMask:styles backing:NSBackingStoreBuffered defer:NO];

    if (!window_)
        return 0;

    [window_ setTitle:[NSString stringWithUTF8String:name]];
    [window_ setReleasedWhenClosed:NO];
    [window_ performSelectorOnMainThread:@selector(makeKeyAndOrderFront:) withObject:nil waitUntilDone:YES];

    [window_ center];

    [NSApp activateIgnoringOtherApps:YES];

    [pool drain];

    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void mfb_close()
{
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

    if (window_)
        [window_ close];

    [pool drain];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int updateEvents()
{
    int state = 0;
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    NSEvent* event = [NSApp nextEventMatchingMask:NSEventMaskAny untilDate:[NSDate distantPast] inMode:NSDefaultRunLoopMode dequeue:YES];
    if (event)
    {
        switch ([event type])
        {
            case NSEventTypeKeyDown:
            case NSEventTypeKeyUp:
            {
                state = -1;
                break;
            }

            default :
            {
                [NSApp sendEvent:event];
                break;
            }
        }
    }
    [pool release];

    if (window_->closed)
        state = -1;

    return state;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int mfb_update(void* buffer)
{
    g_updateBuffer = buffer;
    int state = updateEvents();
    [[window_ contentView] setNeedsDisplay:YES];
    return state;
}
