#include "nestest_check.h"
#include "../neeze/neeze.h"
#include <cstdio>
#include <cstdlib>

static char log_line[256];
static char emu_line[256];
static char emu_regstate[256];
static uint8_t emu_line_cursor = 0;
static neeze::NES_OP_ADDR_MODES emu_addr_mode = neeze::Unused;
static uint8_t emu_instr = 0x0;
static uint32_t emu_ticks = 0;

static void read_file_line(FILE* fp)
{
    fgets(log_line, 256, fp);
}

static void tmp_cb_log(neeze::LOG_LEVEL level, const char* log)
{
    printf("[%s] %s\n", level == neeze::LOG_LEVEL_DEBUG ? "DEBUG" : "\x1B[0;30;41mERROR\x1B[0;;m", log);
}

static void tmp_cb_debug_fetch_instr(neeze::cpu_t& cpu, uint8_t instruction, uint32_t ticks)
{
    // printf("tmp_cb_debug_fetch_instr\n");
    emu_ticks = ticks;
    emu_instr = instruction;
    emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%04X  %02X ", cpu.reg_PC, instruction);
}

static void tmp_cb_debug_addr_mode(neeze::cpu_t& cpu, neeze::NES_OP_ADDR_MODES mode)
{
    // printf("tmp_cb_debug_addr_mode\n");
    emu_addr_mode = mode;
}

static void tmp_cb_debug_mem_write(neeze::cpu_t& cpu, uint16_t addr, uint8_t prev_data, uint8_t new_data)
{
    // printf("= %02X\n", prev_data);
    // printf("tmp_cb_debug_mem_write\n");
    emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "= %02X", prev_data);
}

static void tmp_cb_debug_reg_write(neeze::cpu_t& cpu, char reg)
{
    emu_line_cursor += sprintf(&emu_line[emu_line_cursor], " %c", reg);
}

static void tmp_cb_debug_mem_read(neeze::cpu_t& cpu, uint16_t addr1, uint16_t addr2, uint16_t data)
{
    // printf("tmp_cb_debug_mem_read\n");
    switch (emu_addr_mode)
    {
        case neeze::Const:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s #$%02X ", data, NES_OPS_LUT_SHORT[emu_instr], data);
        break;

        case neeze::Absolute:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X %02X %s $%04X ", (0x00FF & addr1), (0xFF00 & addr1) >> 8, NES_OPS_LUT_SHORT[emu_instr], addr1);
        break;

        case neeze::Absolute_ZP:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s $%02X ", addr1, NES_OPS_LUT_SHORT[emu_instr], 0xFF & addr1);
        break;

        case neeze::Index_X:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X %02X %s $%04X,X @ %04X ", (0x00FF & addr1), (0xFF00 & addr1) >> 8, NES_OPS_LUT_SHORT[emu_instr], addr1, (unsigned short)(addr1 + cpu.reg_X));
        break;

        case neeze::Index_Y:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X %02X %s $%04X,Y @ %04X ", (0x00FF & addr1), (0xFF00 & addr1) >> 8, NES_OPS_LUT_SHORT[emu_instr], addr1, (unsigned short)(addr1 + cpu.reg_Y));
        break;

        case neeze::Index_ZP_X:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s $%02X,X @ %02X ", addr1, NES_OPS_LUT_SHORT[emu_instr], addr1, addr2);
        break;

        case neeze::Index_ZP_Y:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s $%02X,Y @ %02X ", addr1, NES_OPS_LUT_SHORT[emu_instr], 0xFF & addr1, addr2);
        break;

        case neeze::Indirect:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X %02X %s ($%04X) = %04X ", (0x00FF & addr1), (0xFF00 & addr1) >> 8, NES_OPS_LUT_SHORT[emu_instr], data, addr2);
        break;

        case neeze::PreIndex_Indirect_X:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s ($%02X,X) @ %02X = %04X ", addr1, NES_OPS_LUT_SHORT[emu_instr], addr1, (unsigned char)(addr1 + cpu.reg_X), addr2);
        break;

        case neeze::PostIndex_Indirect_Y:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s ($%02X),Y = %04X @ %04X ", addr1, NES_OPS_LUT_SHORT[emu_instr], addr1, addr2, data);
        break;

        case neeze::Relative:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "%02X    %s $%04X ", addr1, NES_OPS_LUT_SHORT[emu_instr], addr2);
        break;

        case neeze::Accumulator:
            data = cpu.reg_A;
        case neeze::No_Address:
        case neeze::Unused:
            emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "      %s", NES_OPS_LUT_SHORT[emu_instr]);
            break;

        default:
            printf("Unknown addressing mode.");
            break;
    };
}

static void tmp_cb_debug_pre_exec(neeze::cpu_t& cpu)
{
    // emu_line_cursor += sprintf(&emu_line[emu_line_cursor], "A:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%3d SL: ---", cpu.reg_A, cpu.reg_X, cpu.reg_Y, cpu.reg_P, cpu.reg_S, 20 % 1000);
    // printf("tmp_cb_debug_pre_exec\n");
    // emu_line_cursor += sprintf(emu_line, "%-48sA:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%3d SL:---", emu_line, cpu.reg_A, cpu.reg_X, cpu.reg_Y, cpu.reg_P, cpu.reg_S, (emu_ticks*3) % 341);
    sprintf(emu_regstate, "A:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%3d SL:---", cpu.reg_A, cpu.reg_X, cpu.reg_Y, cpu.reg_P, cpu.reg_S, (emu_ticks*3) % 341);
}

static void tmp_cb_debug_post_exec(neeze::cpu_t& cpu)
{
    // printf("tmp_cb_debug_post_exec\n");
    // printf("asd: %s\n", emu_regstate);
    emu_line_cursor += sprintf(emu_line, "%-48s%s", emu_line, emu_regstate);
}

static void tmp_cb_debug_instr_done(neeze::cpu_t& cpu)
{
    // printf("done with instruction\n");
}

unsigned int print_diff(uint32_t line_number, char* outbuffer, char* logbuffer)
{
    int c = 0;
    unsigned int misses = 0;
    printf("L%04d ", line_number);
    while (c < 74
        && outbuffer[c] != '\n'
        && logbuffer[c] != '\n'
        && outbuffer[c] != '\0'
        && logbuffer[c] != '\0'
        )
    {
        if (outbuffer[c] != logbuffer[c])
        {
            //_io_printf("-");
#ifdef _WIN32
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | BACKGROUND_RED );
#else
            printf("%c[0;30;41m", 0x1B);
#endif
            misses += 1;
        }

        printf("%c", outbuffer[c]);

        if (outbuffer[c] != logbuffer[c])
        {
#ifdef _WIN32
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE );
#else
            printf("%c[0;;m", 0x1B);
#endif
        }

        c += 1;
    }

    printf("\n");

    return misses;
}

int main(int argc, char const *argv[])
{
    neeze::sys_t sys;
    sys.cb_log = tmp_cb_log;
    sys.cb_debug_fetch_instr = tmp_cb_debug_fetch_instr;
    sys.cb_debug_addr_mode = tmp_cb_debug_addr_mode;
    sys.cb_debug_reg_write = tmp_cb_debug_reg_write;
    sys.cb_debug_mem_write = tmp_cb_debug_mem_write;
    sys.cb_debug_mem_read = tmp_cb_debug_mem_read;
    sys.cb_debug_pre_exec = tmp_cb_debug_pre_exec;
    sys.cb_debug_post_exec = tmp_cb_debug_post_exec;
    sys.cb_debug_instr_done = tmp_cb_debug_instr_done;

    neeze::ines_rom_t rom;
    neeze::init(sys);
    neeze::load_rom_file(sys, "nestest.nes", rom);

    // NOTE about "SL":
    // P is the processor status flag register and SL is the PPU scanline. Note that the nestest "golden" log OR's the status flags with 0x20.

    FILE* logfp = fopen("nestest.log", "r");
    bool run = true;
    bool ok = true;
    uint32_t line_number = 0;
    // while (run)
    while (run && line_number < 8991)
    {
        neeze::step(sys, 1);
        if (line_number < 8991) {
            read_file_line(logfp);
            if (print_diff(++line_number, emu_line, log_line) > 0)
            {
                printf("\nFailed at line line_number: %d\n", line_number);
                printf("EMU: %s\n", emu_line);
                printf("LOG: %s", log_line);
                run = false;
                ok = false;
            }
        } else {
            printf("      %s\n", emu_line);
        }

        emu_line_cursor = 0;
    }

    if (ok) {
        printf("\n\x1B[0;30;42mAll OK!\x1B[0;;m\n");
    } else {
        printf("\n\x1B[0;30;41mFailed!\x1B[0;;m\n");
    }

    fclose(logfp);
}
